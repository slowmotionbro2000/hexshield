# HexShield

[![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/wiki/caption.png)]()[![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/wiki/english.png)](https://gitlab.com/slowmotionbro2000/hexshield/-/blob/main/WIKI_EN.md)[![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/wiki/russian.png)](https://gitlab.com/slowmotionbro2000/hexshield/-/blob/main/WIKI_RU.md)

## Built mod
You can download built mod on [Modrinth](https://modrinth.com/mod/hexshield)

## Requirements
* Minecraft ~1.19.2 
* FabricLoader >=0.14.19
* FabricLanguageKotlin >=1.9.24
* HexCasting >=0.10.3
* HexGloop >=0.1.0
* Libs required by HexCasting & HexGloop

## About mod
This mod is a HexCasting & HexGloop add-on made specially for MineShield: Off-Season

## Features
* Colored Iota list elements made your focus or spellbook description more readable
* HeartWhisper spell will give a lot of love to your friends (or enemies, idk)
* SpectralArrowSupport spell is a powerful weapon (if you have enough amethysts, sure)

## Authors
* DefaultEugene

## License
You can use this mod "as-is" in any non-commercial way. Play with your friends, share this mod or just enjoy!

If you want to use this mod in commercial way, contact me in discord **defaulteugene**

## Support author
[Tinkoff Crowdfunding](https://www.tinkoff.ru/cf/3xpndgVViTU)

[Donation Alerts](https://www.donationalerts.com/r/theeugeneslow)
