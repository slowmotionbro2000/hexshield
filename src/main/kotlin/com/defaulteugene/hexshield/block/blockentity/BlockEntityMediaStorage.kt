package com.defaulteugene.hexshield.block.blockentity

import at.petrak.hexcasting.api.utils.asCompound
import at.petrak.hexcasting.api.utils.extractMedia
import at.petrak.hexcasting.common.lib.HexItems
import com.defaulteugene.hexshield.registry.BlockEntities
import com.defaulteugene.hexshield.utils.MathUtil.pretty
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.ChestBlockEntity
import net.minecraft.entity.Entity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.inventory.Inventories
import net.minecraft.inventory.Inventory
import net.minecraft.inventory.SidedInventory
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtList
import net.minecraft.predicate.entity.EntityPredicates
import net.minecraft.text.Text
import net.minecraft.util.collection.DefaultedList
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Box
import net.minecraft.util.math.Direction
import net.minecraft.world.World
import java.util.*
import java.util.stream.IntStream
import kotlin.math.min
import com.mojang.datafixers.util.Pair
import net.minecraft.block.*
import net.minecraft.network.Packet
import net.minecraft.network.listener.ClientPlayPacketListener
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket
import net.minecraft.util.Formatting

class BlockEntityMediaStorage(pos: BlockPos, state: BlockState): BlockEntity(BlockEntities.mediaStorage, pos, state), Inventory {

    val slots: ArrayList<UUID> = arrayListOf()
    private val items: HashMap<UUID, ItemStack> = hashMapOf()
    private val counts: HashMap<UUID, Long> = hashMapOf()
    var pushBuffer: ItemStack = ItemStack.EMPTY
    var selectedSlot = -1
    var powered: Boolean? = null
        set(value) {
            field = value
            sync()
        }

    private val toDelete: HashSet<UUID> = hashSetOf()

    // interaction methods

    fun nextSlot() {
        if (selectedSlot >= slots.size - 1) {
            selectedSlot = -1
        } else {
            selectedSlot++
        }
        sync()
    }

    fun getCurrentSlotStack(): ItemStack? {
        val uuid = getCurrentSlot() ?: return null
        return items[uuid]
    }

    fun getCurrentSlotCount(): Long? {
        val uuid = getCurrentSlot() ?: return null
        return counts[uuid]
    }

    fun extractStackFromCurrentSlot(): ItemStack? {
        val id = getCurrentSlot() ?: return null
        val item = getCurrentSlotStack() ?: return null
        val maxCount = getCurrentSlotCount() ?: return null

        val count = min(item.maxCount.toLong(), maxCount).toInt()
        if (count >= maxCount)
            markForDelete(id)

        val stack = item.copy()
        stack.count = count
        return stack
    }

    // util methods

    private fun getCurrentSlot(): UUID? {
        if (selectedSlot >= slots.size) {
            selectedSlot = -1
        }

        if (selectedSlot < 0)
            return null

        return slots[selectedSlot]
    }

    private fun markForDelete(slot: UUID) {
        toDelete.add(slot)
    }

    private fun clearInner() {
        slots.clear()
        items.clear()
        counts.clear()
    }

    private fun sync() {
        this.markDirty()
        val state = world!!.getBlockState(pos!!)
        world!!.updateListeners(pos, state, state, Block.NOTIFY_ALL)
    }

    private fun getTotalMedia(): Long {
        var media: Long = 0
        for (slot in slots) {
            val item = items[slot]!!
            val count = counts[slot]!!

            val cost = extractMedia(item, cost = -1, drainForBatteries = false, simulate = true)
            media += cost * count
        }
        return media
    }

    fun toItemStack(): ItemStack {
        val stack = ItemStack(com.defaulteugene.hexshield.registry.Blocks.mediaStorage)

        val internalTag = NbtCompound()
        writeToNbt(internalTag)

        val stackTag = NbtCompound()
        stack.nbt = stackTag
        stackTag.put("internal_storage", internalTag)

        return stack
    }

    fun fromItemStack(stack: ItemStack) {
        val internals = getInternalStorageTag(stack) ?: return
        readFromNbt(internals)
    }

    // tick methods

    private fun invalidateSlots() {
        toDelete.forEach{ slot -> run {
            slots.remove(slot)
            items.remove(slot)
            counts.remove(slot)
        }}

        toDelete.clear()
    }

    fun updateBuffer() {
        if (pushBuffer.isEmpty)
            return

        for (slot in slots) {
            if (!counts.containsKey(slot) || !items.containsKey(slot)) {
                markForDelete(slot)
                continue
            }

            if (ItemStack.canCombine(items[slot], pushBuffer)) {
                counts[slot] = counts[slot]!! + pushBuffer.count
                pushBuffer.count = 0
                return
            }
        }

        if (slots.size >= 1024)
            return

        val newId = UUID.randomUUID()
        slots.add(newId)
        items[newId] = pushBuffer.copy()
        items[newId]!!.count = 1
        counts[newId] = pushBuffer.count.toLong()

        pushBuffer.count = 0
        sync()
    }

    fun updatePusher() {
        val slotId = getCurrentSlot() ?: return
        val insertStack = getCurrentSlotStack() ?: return
        val totalItems = getCurrentSlotCount() ?: return

        if (insertStack.isEmpty || totalItems <= 0) return

        val world = this.world ?: return
        val to = getInventoryAt(world, this.pos.down()) ?: return

        val isSided = to is SidedInventory
        val availableSlots =
            if (isSided) IntStream.of(*(to as SidedInventory).getAvailableSlots(Direction.UP))
            else IntStream.range(0, to.size())

        var leftover = insertStack.copy()
        val insertLimit = min(leftover.maxCount, min(totalItems, Int.MAX_VALUE.toLong()).toInt()).toLong()
        leftover.count = insertLimit.toInt()

        for (toSlot in availableSlots) {
            if (leftover.isEmpty) break
            if (!canInsert(to, toSlot, leftover))
                continue

            leftover = insert(to, toSlot, stack = leftover)
        }

        val extracted = insertLimit - leftover.count
        if (extracted >= totalItems) {
            markForDelete(slotId)
        } else {
            counts[slotId] = counts[slotId]!! - extracted
        }
    }

    companion object {
        fun tick(world: World, pos: BlockPos, state: BlockState, entity: BlockEntityMediaStorage) {
            val slotBeforeUpdate = entity.selectedSlot
            val itemBeforeUpdate = entity.getCurrentSlotStack()
            val countBeforeUpdate = entity.getCurrentSlotCount()

            if (entity.powered == null) {
                entity.powered = world.isReceivingRedstonePower(pos)
            }

            entity.invalidateSlots()
            entity.updateBuffer()
            if (entity.powered != null && !entity.powered!!)
                entity.updatePusher()
            if (entity.selectedSlot > entity.slots.size)
                entity.selectedSlot = -1

            if (slotBeforeUpdate != entity.selectedSlot
                || itemBeforeUpdate != entity.getCurrentSlotStack()
                || countBeforeUpdate != entity.getCurrentSlotCount())
                entity.sync()
        }

        fun applyLensOverlay(
            lines: MutableList<Pair<ItemStack, Text>>,
            pos: BlockPos,
            world: World,
        ) {
            val tile = world.getBlockEntity(pos)
            if (tile !is BlockEntityMediaStorage) return

            lines.add(Pair(ItemStack(HexItems.CHARGED_AMETHYST), Text.translatable("block.hexshield.media_storage.total_media").append(Text.literal(tile.getTotalMedia().pretty))))

            if (tile.selectedSlot < 0 || tile.selectedSlot >= tile.slots.size) {
                lines.add(Pair(ItemStack(Blocks.CHEST), Text.translatable("block.hexshield.media_storage.page.none")))
                lines.add(Pair(ItemStack(Items.PAPER), Text.translatable("block.hexshield.media_storage.page.count", tile.slots.size)))
                if (tile.powered != null && tile.powered!!) {
                    lines.add(Pair(ItemStack(Items.REDSTONE_TORCH), Text.translatable("block.hexshield.media_storage.powered")))
                }
                return
            }

            val currentSlotStack = tile.getCurrentSlotStack()
            val currentSlotCount = tile.getCurrentSlotCount()

            if (currentSlotStack == null) {
                lines.add(Pair(ItemStack(Blocks.BARRIER), Text.translatable(
                    "block.hexshield.media_storage.stack",
                    Text.translatable("block.hexshield.media_storage.unknown").styled { style -> style.withColor(Formatting.RED) },
                    currentSlotCount
                )))
                lines.add(Pair(ItemStack(Items.PAPER), Text.translatable("block.hexshield.media_storage.page", tile.selectedSlot + 1, tile.slots.size)))
                if (tile.powered != null && tile.powered!!) {
                    lines.add(Pair(ItemStack(Items.REDSTONE_TORCH), Text.translatable("block.hexshield.media_storage.powered")))
                }
                return
            }

            lines.add(Pair(currentSlotStack, Text.translatable("block.hexshield.media_storage.stack", currentSlotStack.toHoverableText(), currentSlotCount)))
            lines.add(Pair(ItemStack(Items.PAPER), Text.translatable("block.hexshield.media_storage.page", tile.selectedSlot + 1, tile.slots.size)))
            if (tile.powered != null && tile.powered!!) {
                lines.add(Pair(ItemStack(Items.REDSTONE_TORCH), Text.translatable("block.hexshield.media_storage.powered")))
            }
        }

        fun getInternalStorageTag(stack: ItemStack): NbtCompound? {
            val stackTag = stack.nbt ?: return null
            var internalTag = if (stackTag.contains("internal_storage")) stackTag.getCompound("internal_storage") else null

            if (internalTag == null) run beTagFind@{
                if (!stackTag.contains("BlockEntityTag")) return null
                val beTag = stackTag.getCompound("BlockEntityTag")

                if (!beTag.contains("Items")) return null
                val beItems = beTag.get("Items")

                if (beItems is NbtList)
                    for (beItem in beItems) {
                        if (!beItem.asCompound.contains("tag")) continue
                        val compound = beItem.asCompound.getCompound("tag")

                        if (compound.contains("internal_storage")) {
                            internalTag = compound.getCompound("internal_storage")
                            return@beTagFind
                        }
                    }
            }

            return internalTag
        }

        private fun canInsert(to: Inventory, slot: Int, stack: ItemStack?): Boolean {
            if (!to.isValid(slot, stack))
                return false
            return to !is SidedInventory || to.canInsert(slot, stack, Direction.UP)
        }

        private fun insert(to: Inventory, slot: Int, stack: ItemStack): ItemStack {
            val containing = to.getStack(slot)

            if (containing.isEmpty) {
                to.setStack(slot, stack)
                to.markDirty()
                return ItemStack.EMPTY
            }

            if (containing.count >= containing.maxCount)
                return stack

            if (!ItemStack.canCombine(containing, stack))
                return stack

            if (containing.count + stack.count <= containing.maxCount) {
                containing.count += stack.count
                stack.count = 0
                return ItemStack.EMPTY
            }

            val toInsert = min(containing.maxCount - containing.count, stack.count)
            stack.decrement(toInsert)
            containing.increment(toInsert)

            return stack
        }

        private fun getInventoryAt(world: World, blockPos: BlockPos): Inventory? {
            var inventory: Inventory? = null
            val blockState = world.getBlockState(blockPos)
            val block = blockState.block
            if (block is InventoryProvider) {
                inventory = (block as InventoryProvider).getInventory(blockState, world, blockPos)
            } else if (blockState.hasBlockEntity()) {
                val blockEntity = world.getBlockEntity(blockPos)
                if (blockEntity is Inventory) {
                    inventory = blockEntity
                    if (inventory is ChestBlockEntity && block is ChestBlock) {
                        inventory = ChestBlock.getInventory(block, blockState, world, blockPos, true)
                    }
                }
            }

            if (inventory == null) {
                val list = world.getOtherEntities(
                    null as Entity?,
                    Box(blockPos.x - 0.5, blockPos.y - 0.5, blockPos.z - 0.5, blockPos.x + 0.5, blockPos.y + 0.5, blockPos.z + 0.5),
                    EntityPredicates.VALID_INVENTORIES
                )
                if (list.isNotEmpty()) {
                    inventory = list[world.random.nextInt(list.size)] as Inventory
                }
            }

            return inventory
        }
    }

    // important overrides

    override fun isValid(slot: Int, stack: ItemStack?): Boolean {
        return slot == 0
                && stack != null
                && stack.isStackable
                && !stack.isDamageable
                && extractMedia(stack, cost = -1, drainForBatteries = false, simulate = true) > 0
    }

    override fun readNbt(nbt: NbtCompound?) {
        super.readNbt(nbt)
        nbt ?: return

        readFromNbt(nbt)
    }

    public override fun writeNbt(nbt: NbtCompound?) {
        super.writeNbt(nbt)
        nbt ?: return

        writeToNbt(nbt)
    }

    private fun readFromNbt(nbt: NbtCompound) {
        clearInner()

        this.selectedSlot = nbt.getInt("SelectedSlot")

        val nbtList = nbt.getList("InnerItems", 10)

        for (i in nbtList.indices) {
            val nbtCompound = nbtList.getCompound(i)
            val slotId = nbtCompound.getUuid("SlotUUID")
            val count = nbtCompound.getLong("RealCount")
            val stack = ItemStack.fromNbt(nbtCompound)

            slots.add(slotId)
            items[slotId] = stack
            counts[slotId] = count
        }

        val buffer = DefaultedList.ofSize(1, ItemStack.EMPTY)
        Inventories.readNbt(nbt, buffer)

        pushBuffer = buffer[0]
        world ?: return
        if (this.world!!.isClient && nbt.contains("IsPowered")) {
            powered = nbt.getBoolean("IsPowered")
        }
    }

    private fun writeToNbt(nbt: NbtCompound) {
        nbt.putInt("SelectedSlot", this.selectedSlot)

        val nbtList = NbtList()

        slots.forEach { slot -> run {
            val element = NbtCompound()

            val item = items[slot] ?: return@run
            val count = counts[slot] ?: return@run

            element.putUuid("SlotUUID", slot)
            element.putLong("RealCount", count)
            item.writeNbt(element)

            nbtList.add(element)
        }}

        nbt.put("InnerItems", nbtList)

        Inventories.writeNbt(nbt, DefaultedList.ofSize(1, pushBuffer))

        this.powered ?: return
        nbt.putBoolean("IsPowered", this.powered!!)
    }

    override fun toInitialChunkDataNbt(): NbtCompound {
        val tag = NbtCompound()
        writeNbt(tag)
        return tag
    }

    override fun toUpdatePacket(): Packet<ClientPlayPacketListener>? {
        return BlockEntityUpdateS2CPacket.create(this) { be ->
            if (be is BlockEntityMediaStorage) {
                val tag = NbtCompound()
                be.writeNbt(tag)
                return@create tag
            }
            be.toInitialChunkDataNbt()
        }
    }

    // misc overrides

    override fun clear() {
        pushBuffer = ItemStack.EMPTY
    }

    override fun size(): Int {
        return 1
    }

    override fun isEmpty(): Boolean {
        return pushBuffer.isEmpty
    }

    override fun getStack(slot: Int): ItemStack {
        return pushBuffer
    }

    override fun removeStack(slot: Int, amount: Int): ItemStack? {
        return Inventories.splitStack(arrayListOf(pushBuffer), slot, amount)
    }

    override fun removeStack(slot: Int): ItemStack? {
        return Inventories.removeStack(arrayListOf(pushBuffer), slot)
    }

    override fun setStack(slot: Int, stack: ItemStack?) {
        pushBuffer = stack ?: return
        sync()
    }

    override fun canPlayerUse(player: PlayerEntity?): Boolean = false
}