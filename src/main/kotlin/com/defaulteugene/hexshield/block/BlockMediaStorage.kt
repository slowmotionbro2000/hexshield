package com.defaulteugene.hexshield.block

import at.petrak.hexcasting.api.utils.extractMedia
import com.defaulteugene.hexshield.Reference
import com.defaulteugene.hexshield.block.blockentity.BlockEntityMediaStorage
import com.defaulteugene.hexshield.registry.BlockEntities
import com.defaulteugene.hexshield.utils.MathUtil.pretty
import net.minecraft.block.Block
import net.minecraft.block.BlockRenderType
import net.minecraft.block.BlockState
import net.minecraft.block.BlockWithEntity
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityTicker
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.block.piston.PistonBehavior
import net.minecraft.client.item.TooltipContext
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.ItemStack
import net.minecraft.loot.context.LootContext
import net.minecraft.loot.context.LootContextParameters
import net.minecraft.nbt.NbtList
import net.minecraft.text.Text
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.Identifier
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.world.BlockView
import net.minecraft.world.World

class BlockMediaStorage(settings: Settings) : BlockWithEntity(settings.nonOpaque()) {

    @Deprecated("Deprecated in Java", ReplaceWith("PistonBehavior.BLOCK", "net.minecraft.block.piston.PistonBehavior"))
    override fun getPistonBehavior(state: BlockState?): PistonBehavior = PistonBehavior.BLOCK

    override fun createBlockEntity(pos: BlockPos?, state: BlockState?): BlockEntity? {
        if (pos == null || state == null)
            return null
        return BlockEntityMediaStorage(pos, state)
    }

    override fun onPlaced(
        world: World?,
        pos: BlockPos?,
        state: BlockState?,
        placer: LivingEntity?,
        itemStack: ItemStack?
    ) {
        super.onPlaced(world, pos, state, placer, itemStack)
        world ?: return
        itemStack ?: return

        val blockEntity = world.getBlockEntity(pos)
        if (blockEntity is BlockEntityMediaStorage) {
            blockEntity.fromItemStack(itemStack)
        }
    }

    @Deprecated(
        "Deprecated in Java",
        ReplaceWith("super.getDroppedStacks(state, builder)", "net.minecraft.block.BlockWithEntity")
    )
    override fun getDroppedStacks(state: BlockState?, builder: LootContext.Builder?): MutableList<ItemStack> {
        state ?: return super.getDroppedStacks(null, builder)
        builder ?: return super.getDroppedStacks(state, null)

        val blockEntity = builder.getNullable(LootContextParameters.BLOCK_ENTITY)
        if (blockEntity !is BlockEntityMediaStorage)
            return super.getDroppedStacks(state, builder)

        builder.putDrop(Identifier(Reference.MOD_ID, "media_storage_drop")) { _, consumer ->
            consumer.accept(blockEntity.toItemStack())
        }

        return super.getDroppedStacks(state, builder)
    }


    override fun getPickStack(world: BlockView?, pos: BlockPos?, state: BlockState?): ItemStack {
        if (world == null || pos == null)
            return super.getPickStack(world, pos, state)

        val blockEntity = world.getBlockEntity(pos)
        if (blockEntity is BlockEntityMediaStorage) {
            return blockEntity.toItemStack()
        }
        return super.getPickStack(world, pos, state)
    }

    @Deprecated(
        "Deprecated in Java", ReplaceWith(
            "super.onUse(state, world, pos, player, hand, hit)",
            "net.minecraft.block.BlockWithEntity"
        )
    )
    override fun onUse(
        state: BlockState?,
        world: World?,
        pos: BlockPos?,
        player: PlayerEntity?,
        hand: Hand?,
        hit: BlockHitResult?
    ): ActionResult {
        world ?: return ActionResult.FAIL
        pos ?: return ActionResult.FAIL

        if (world.isClient)
            return ActionResult.SUCCESS

        val blockEntity = world.getBlockEntity(pos)
        if (blockEntity !is BlockEntityMediaStorage)
            return ActionResult.FAIL

        if (player != null) {
            val heldItem = player.inventory.getStack(player.inventory.selectedSlot)
            if (blockEntity.pushBuffer.isEmpty && extractMedia(heldItem, -1, drainForBatteries = false, simulate = true) > 0) {
                blockEntity.pushBuffer = heldItem.copy()
                heldItem.count = 0
                return ActionResult.SUCCESS
            }
        }

        blockEntity.nextSlot()
        return ActionResult.SUCCESS
    }

    override fun <T : BlockEntity?> getTicker(
        world: World?,
        state: BlockState?,
        type: BlockEntityType<T>?
    ): BlockEntityTicker<T>? {
        if (world == null || world.isClient()) return null
        return checkType(type, BlockEntities.mediaStorage, BlockEntityMediaStorage::tick)
    }

    override fun appendTooltip(
        stack: ItemStack?,
        world: BlockView?,
        tooltip: MutableList<Text>?,
        options: TooltipContext?
    ) {
        super.appendTooltip(stack, world, tooltip, options)
        stack ?: return
        tooltip ?: return

        val internalStorageTag = BlockEntityMediaStorage.getInternalStorageTag(stack) ?: return
        if (internalStorageTag.contains("InnerItems")) {
            val inner = internalStorageTag.get("InnerItems")
            if (inner !is NbtList) return

            tooltip.add(Text.translatable("block.hexshield.media_storage.desc").append(inner.size.pretty))
        }

    }

    @Deprecated("Deprecated in Java", ReplaceWith("BlockRenderType.MODEL", "net.minecraft.block.BlockRenderType"))
    override fun getRenderType(state: BlockState?): BlockRenderType = BlockRenderType.MODEL

    @Deprecated("Deprecated in Java", ReplaceWith("true"))
    override fun hasSidedTransparency(state: BlockState?): Boolean = true

    @Deprecated("Deprecated in Java")
    override fun neighborUpdate(
        state: BlockState?,
        world: World,
        pos: BlockPos?,
        sourceBlock: Block?,
        sourcePos: BlockPos?,
        notify: Boolean
    ) {
        super.neighborUpdate(state, world, pos, sourceBlock, sourcePos, notify)
        if (world.isClient) return

        val blockEntity = world.getBlockEntity(pos)
        if (blockEntity !is BlockEntityMediaStorage) return

        blockEntity.powered ?: return
        val newSignal: Boolean = world.isReceivingRedstonePower(pos)

        blockEntity.powered = newSignal
    }
}