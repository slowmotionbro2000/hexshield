package com.defaulteugene.hexshield.render.entity

import com.defaulteugene.hexshield.Reference
import com.defaulteugene.hexshield.entity.EntityShadowArrow
import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.client.render.entity.EntityRendererFactory
import net.minecraft.client.render.entity.ProjectileEntityRenderer
import net.minecraft.util.Identifier

@Environment(EnvType.CLIENT)
class RenderShadowArrow(context: EntityRendererFactory.Context) : ProjectileEntityRenderer<EntityShadowArrow>(context) {

    override fun getTexture(entity: EntityShadowArrow?): Identifier = texture

    companion object {
        val texture: Identifier = Identifier(Reference.MOD_ID, "textures/entity/projectiles/shadow_arrow.png")
    }
}