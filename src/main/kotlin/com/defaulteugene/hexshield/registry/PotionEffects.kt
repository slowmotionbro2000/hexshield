package com.defaulteugene.hexshield.registry

import com.defaulteugene.hexshield.entity.statusEffects.sactificialContract.*
import net.minecraft.entity.effect.StatusEffect

object PotionEffects : AbstractRegistry<StatusEffect>() {

    val effectVampireContract : StatusEffect = register("vampire_contract", VampireContractEffect)
    val effectEpiphanyContract : StatusEffect = register("epiphany_contract", EpiphanyContractEffect)
    val effectResistanceContract : StatusEffect = register("resistance_contract", ResistanceContractEffect)
    val effectEverwingsContract : StatusEffect = register("everwings_contract", EverwingsContractEffect)
    val effectCursedContract : StatusEffect = register("cursed_contract", CursedContractEffect)
    val effectRangeContract : StatusEffect = register("range_contract", RangeContractEffect)

}