package com.defaulteugene.hexshield.registry

import com.defaulteugene.hexshield.block.blockentity.BlockEntityMediaStorage
import net.fabricmc.fabric.api.`object`.builder.v1.block.entity.FabricBlockEntityTypeBuilder
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.util.math.BlockPos
import java.util.function.BiFunction

object BlockEntities: AbstractRegistry<BlockEntityType<*>>() {

    val mediaStorage: BlockEntityType<BlockEntityMediaStorage> = wrap(
        "media_storage",
        { pos, state -> BlockEntityMediaStorage(pos, state) },
        arrayOf(Blocks.mediaStorage)
    )

    private fun <T: BlockEntity> wrap(
        id: String,
        supplier: BiFunction<BlockPos, BlockState, T>,
        blocks: Array<Block>
    ): BlockEntityType<T> {
        val ret = FabricBlockEntityTypeBuilder.create(supplier::apply, *blocks).build()
        register(id, ret)
        return ret
    }
}