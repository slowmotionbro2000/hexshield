package com.defaulteugene.hexshield.registry

import com.defaulteugene.hexshield.Reference
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import java.util.function.BiConsumer

abstract class AbstractRegistry<T> {

    protected val items: LinkedHashMap<Identifier, T> = LinkedHashMap()

    private fun <T> bind(registry: Registry<in T>): BiConsumer<Identifier, T> =
        BiConsumer<Identifier, T> { id, item -> Registry.register(registry, id, item) }

    fun <U : T> register(id: String, item: U): U {
        val rs = Identifier(Reference.MOD_ID, id)

        if (items.containsKey(rs))
            throw IllegalArgumentException("Typo? Duplicate id \"${Reference.MOD_ID}:${id}\" in ${javaClass.name}")

        items[rs] = item

        return item
    }

    open fun initRegistry(registry: Registry<T>) {
        val consumer = bind(registry)
        items.forEach(consumer)
    }
}