package com.defaulteugene.hexshield.registry

import com.defaulteugene.hexshield.entity.EntityShadowArrow
import net.fabricmc.fabric.api.`object`.builder.v1.entity.FabricEntityTypeBuilder
import net.minecraft.entity.EntityType
import net.minecraft.entity.SpawnGroup

object Entities: AbstractRegistry<EntityType<*>>() {

    val shadowArrow: EntityType<EntityShadowArrow> = register(
        "shadow_arrow",
        FabricEntityTypeBuilder.create(SpawnGroup.MISC) { type, world -> EntityShadowArrow(type, world) }.build()
    )

}