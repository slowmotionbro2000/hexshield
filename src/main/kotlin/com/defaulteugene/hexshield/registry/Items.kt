package com.defaulteugene.hexshield.registry

import at.petrak.hexcasting.api.misc.MediaConstants
import com.defaulteugene.hexshield.HexShield
import com.defaulteugene.hexshield.item.ItemMisc
import com.defaulteugene.hexshield.item.ItemPhilosopherMediaBattery
import com.defaulteugene.hexshield.item.ItemShadowArrow
import com.defaulteugene.hexshield.item.ItemSimpleMediaProvider
import net.minecraft.item.Item
import net.minecraft.item.Item.Settings
import net.minecraft.util.Rarity

object Items: AbstractRegistry<Item>() {

    // battery items
    val itemPhilosopherMediaBattery : ItemPhilosopherMediaBattery = register(
        "philosopher_battery", ItemPhilosopherMediaBattery(unstackable())
    )

    // media items
    val wardenShard : ItemSimpleMediaProvider = register(
        "wardenyst", ItemSimpleMediaProvider(settings(), MediaConstants.CRYSTAL_UNIT * 8, 500, Rarity.UNCOMMON)
    )
    val dragonShard : ItemSimpleMediaProvider = register(
        "dragonyst", ItemSimpleMediaProvider(settings(), MediaConstants.CRYSTAL_UNIT * 64, 400, Rarity.EPIC)
    )

    // misc items
    val wardenSoul : ItemMisc = register("warden_soul", ItemMisc(settings(), Rarity.UNCOMMON))
    val dragonSoul : ItemMisc = register("dragon_soul", ItemMisc(settings(), Rarity.EPIC))

    val shadowArrow : ItemShadowArrow = register("shadow_arrow", ItemShadowArrow(settings()))

    fun settings(): Settings {
        return Settings().group(HexShield.getCreativeTab())
    }

    private fun unstackable(): Settings {
        return settings().maxCount(1)
    }
}