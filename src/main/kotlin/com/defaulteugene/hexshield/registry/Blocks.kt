package com.defaulteugene.hexshield.registry

import com.defaulteugene.hexshield.block.BlockMediaStorage
import net.minecraft.block.AbstractBlock.Settings
import net.minecraft.block.Block
import net.minecraft.block.Material
import net.minecraft.item.BlockItem
import net.minecraft.item.Item
import net.minecraft.sound.BlockSoundGroup
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import java.util.function.BiConsumer
import kotlin.jvm.Throws

object Blocks: AbstractRegistry<Block>() {

    val mediaStorage: BlockMediaStorage = register("media_storage", BlockMediaStorage(resistant()))

    private fun settings(): Settings {
        return Settings.of(Material.STONE)
            .sounds(BlockSoundGroup.BASALT)
            .strength(2f, 4f)
    }

    private fun resistant(): Settings = settings().resistance(1_000_000f)

    private fun bind(blockRegistry: Registry<Block>, itemRegistry: Registry<Item>): BiConsumer<Identifier, Block>
        = BiConsumer<Identifier, Block> {
            id, block -> run {
                Registry.register(blockRegistry, id, block)
                Registry.register(itemRegistry, id, BlockItem(block, Items.settings()))
            }
        }

    fun initRegistry(blockRegistry: Registry<Block>, itemRegistry: Registry<Item>) {
        val consumer = bind(blockRegistry, itemRegistry)
        return items.forEach(consumer)
    }

    @Throws(NotImplementedError::class)
    @Deprecated(
        "This method is not implemented for this registry",
        ReplaceWith(
            "Blocks.initRegistry(Registry.BLOCK, Registry.ITEM)",
            imports = arrayOf(
                "com.defaulteugene.hexshield.registry.Blocks",
                "net.minecraft.util.registry.Registry"
            )
        ),
        level = DeprecationLevel.ERROR
    )
    override fun initRegistry(registry: Registry<Block>) {
        throw NotImplementedError("Method initRegistry(Registry<Block>) not implemented for blocks registry. Use initRegistry(Registry<Block>, Registry<Item>) instead")
    }
}