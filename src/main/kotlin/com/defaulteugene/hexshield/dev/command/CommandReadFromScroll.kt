package com.defaulteugene.hexshield.dev.command

import at.petrak.hexcasting.api.spell.iota.Iota
import at.petrak.hexcasting.api.spell.iota.PatternIota
import at.petrak.hexcasting.common.items.ItemScroll
import com.defaulteugene.hexshield.command.AbstractCommand
import com.mojang.brigadier.builder.LiteralArgumentBuilder
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.server.world.ServerWorld
import net.minecraft.text.Text

object CommandReadFromScroll : AbstractCommand() {
    override fun getName(): String = "read_pattern"

    override fun getPermissionLevel(): Int = 0

    override fun getDedicationType(): EnumDedicationType = EnumDedicationType.ANY

    private fun execute(source: ServerCommandSource): Int {
        if (source.entity !is PlayerEntity)
            throw createException("Command may be only used by player")

        val heldItem = (source.entity as PlayerEntity).inventory.mainHandStack
        if (heldItem.item !is ItemScroll)
            throw createException("You need to held ItemScroll in main hand")

        val holder = heldItem.item as ItemScroll
        val iota: Iota? = holder.readIota(heldItem, (source.entity as PlayerEntity).world as ServerWorld)

        if (iota is PatternIota) {
            (source.entity as PlayerEntity).sendMessage(Text.literal(iota.pattern.anglesSignature() + " / " + iota.pattern.startDir))
            return 0
        }

        throw createException("Scroll does not contain PatternIota")
    }

    override fun registerCommandArguments(builder: LiteralArgumentBuilder<ServerCommandSource>): LiteralArgumentBuilder<ServerCommandSource> {
        return builder.executes { context -> execute(context.source) }
    }
}