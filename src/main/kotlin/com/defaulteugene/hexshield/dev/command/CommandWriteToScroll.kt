package com.defaulteugene.hexshield.dev.command

import at.petrak.hexcasting.api.spell.iota.Iota
import at.petrak.hexcasting.api.spell.iota.PatternIota
import at.petrak.hexcasting.api.spell.math.HexDir
import at.petrak.hexcasting.api.spell.math.HexPattern
import at.petrak.hexcasting.common.items.ItemScroll
import com.defaulteugene.hexshield.command.AbstractCommand
import com.mojang.brigadier.arguments.StringArgumentType
import com.mojang.brigadier.builder.LiteralArgumentBuilder
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.server.command.CommandManager
import net.minecraft.server.command.ServerCommandSource

object CommandWriteToScroll : AbstractCommand() {
    override fun getName(): String
        = "write_pattern"

    override fun getPermissionLevel(): Int
        = 0

    override fun getDedicationType(): EnumDedicationType
        = EnumDedicationType.ANY

    private fun execute(source: ServerCommandSource, pattern: String, direction: String): Int {
        if (source.entity !is PlayerEntity)
            throw createException("Command may be only used by player")

        val heldItem = (source.entity as PlayerEntity).inventory.mainHandStack
        if (heldItem.item !is ItemScroll)
            throw createException("You need to held ItemScroll in main hand")

        val holder = heldItem.item as ItemScroll
        val iota: Iota

        try {
            iota = PatternIota(HexPattern.fromAngles(pattern, HexDir.fromString(direction)))
        } catch (iae: IllegalArgumentException) {
            throw createException("Given pattern might not exist")
        }

        if (holder.canWrite(heldItem, iota)) {
            holder.writeDatum(heldItem, iota)
            return 0
        }

        throw createException("Can't write given pattern to held item")
    }

    override fun registerCommandArguments(builder: LiteralArgumentBuilder<ServerCommandSource>): LiteralArgumentBuilder<ServerCommandSource> {
        return builder
            .then(CommandManager.argument("hexdirections", StringArgumentType.string())
            .then(CommandManager.argument("hexstart", StringArgumentType.string())
                .executes {context -> execute(
                        context.source,
                        context.getArgument("hexdirections", String::class.java),
                        context.getArgument("hexstart", String::class.java)
                )
                }))
    }
}