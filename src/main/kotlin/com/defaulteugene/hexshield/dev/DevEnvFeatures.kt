package com.defaulteugene.hexshield.dev

import com.defaulteugene.hexshield.dev.command.CommandReadFromScroll
import com.defaulteugene.hexshield.dev.command.CommandWriteToScroll
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback

// this class excluded from build jar and called by reflection
object DevEnvFeatures {

    @JvmStatic
    fun register() {
        CommandRegistrationCallback.EVENT.register { dispatcher, _, _ ->
            run {
                CommandWriteToScroll.register(dispatcher, true)
                CommandReadFromScroll.register(dispatcher, true)
            }
        }
    }

}