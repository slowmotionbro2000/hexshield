package com.defaulteugene.hexshield

import at.petrak.hexcasting.api.client.ScryingLensOverlayRegistry
import com.defaulteugene.hexshield.block.BlockMediaStorage
import com.defaulteugene.hexshield.block.blockentity.BlockEntityMediaStorage
import com.defaulteugene.hexshield.command.client.CommandHexDump
import com.defaulteugene.hexshield.registry.Entities
import com.defaulteugene.hexshield.render.entity.RenderShadowArrow
import com.mojang.datafixers.util.Pair
import net.fabricmc.api.ClientModInitializer
import net.fabricmc.fabric.api.client.command.v2.ClientCommandRegistrationCallback
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry
import net.minecraft.block.BlockState
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.ItemStack
import net.minecraft.text.Text
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.world.World

object HexShieldClient : ClientModInitializer {
    override fun onInitializeClient() {

        ClientCommandRegistrationCallback.EVENT.register {
            dispatcher, _ -> run {
                dispatcher.register(CommandHexDump.getBuilder())
            }
        }

        ScryingLensOverlayRegistry.addPredicateDisplayer(
            { state: BlockState, _: BlockPos?, _: PlayerEntity?, _: World?, _: Direction? ->
                state.block is BlockMediaStorage },
            { lines: MutableList<Pair<ItemStack, Text>>, _: BlockState?, pos: BlockPos, _: PlayerEntity?, world: World, _: Direction? ->
                BlockEntityMediaStorage.applyLensOverlay(lines, pos, world) }
        )

        EntityRendererRegistry.register(Entities.shadowArrow) { context -> RenderShadowArrow(context) }
    }
}