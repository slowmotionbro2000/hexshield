package com.defaulteugene.hexshield.utils.iota_images

import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import java.awt.Color
import java.awt.Font
import java.awt.Graphics2D
import java.awt.image.BufferedImage

@Environment(EnvType.CLIENT)
abstract class AbstractAdapter {

    protected lateinit var image: BufferedImage
    protected lateinit var graphics: Graphics2D

    protected open fun getFieldColor(): Color = Color.BLUE
    protected open fun getMainInfoColor(): Color = Color.BLUE
    protected open fun getAdditionalInfoColor(): Color = Color.BLUE

    protected open fun preDraw() {
        val pair = prepareImageField(getFieldColor())
        image = pair.first
        graphics = pair.second
    }

    abstract fun draw()
    abstract fun postDraw()

    open fun createImage(): BufferedImage {
        preDraw()
        draw()
        postDraw()

        return image
    }

    companion object {

        protected fun prepareImageField(border: Color): Pair<BufferedImage, Graphics2D> {
            val image = BufferedImage(1024, 1024, BufferedImage.TYPE_INT_ARGB)
            val graphics = image.createGraphics()

            graphics.color = Color.WHITE
            graphics.fillRect(0, 0, 1024, 1024)
            graphics.color = border

            graphics.drawLine(16, 16, 16, 1024 - 16)
            graphics.drawLine(16, 1024 - 16, 1024 - 16, 1024 - 16)
            graphics.drawLine(1024 - 16, 1024 - 16, 1024 - 16, 16)
            graphics.drawLine(1024 - 16, 16, 16, 16)

            return Pair(image, graphics)
        }

        @JvmStatic
        protected fun appendInfo(graphics: Graphics2D, name: String, mod: String, additionalData: String, mainColor: Color = Color.BLUE, additionDataColor: Color) {
            val prevFont = graphics.font

            graphics.font = Font(prevFont.fontName, prevFont.style, 40)
            val width = graphics.fontMetrics.stringWidth(name)

            graphics.color = Color.WHITE
            graphics.fillRect(512 - width / 2 - 10, 1024 - 17, width + 20, 2)

            graphics.color = mainColor
            graphics.drawString(name, 512 - width / 2, 1024 - 4)

            graphics.font = Font(prevFont.fontName, prevFont.style, 25)
            graphics.drawString(mod, 20, 1024 - 20)

            graphics.color = additionDataColor
            graphics.drawString(additionalData, 1024 - 32 - graphics.fontMetrics.stringWidth(additionalData), 48)

            graphics.font = prevFont
        }

    }
}