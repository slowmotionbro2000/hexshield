package com.defaulteugene.hexshield.utils.iota_images

import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.util.Formatting
import java.awt.BasicStroke
import java.awt.Color

@Environment(EnvType.CLIENT)
class BraceAdapter private constructor(private val left: Boolean) : AbstractAdapter() {

    companion object {
        val left = BraceAdapter(true)
        val right = BraceAdapter(false)
    }

    private val color = Color(Formatting.RED.colorValue!!)

    override fun getFieldColor(): Color = color
    override fun getAdditionalInfoColor(): Color = color

    override fun draw() {
        val prevStroke = graphics.stroke

        graphics.stroke = BasicStroke(10F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND)

        if (left) {
            graphics.drawLine(512 - 128, 256, 512 - 128, 1024 - 256)
        } else {
            graphics.drawLine(512 + 128, 256, 512 + 128, 1024 - 256)
        }

        graphics.drawLine(512 - 128, 256, 512 + 128, 256)
        graphics.drawLine(512 - 128, 1024 - 256, 512 + 128, 1024 - 256)

        graphics.stroke = prevStroke
    }

    override fun postDraw() {
    }
}