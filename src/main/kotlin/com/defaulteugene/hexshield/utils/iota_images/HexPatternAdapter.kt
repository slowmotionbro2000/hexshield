package com.defaulteugene.hexshield.utils.iota_images

import at.petrak.hexcasting.api.PatternRegistry
import at.petrak.hexcasting.api.spell.ConstMediaAction
import at.petrak.hexcasting.api.spell.math.HexPattern
import at.petrak.hexcasting.api.spell.mishaps.MishapInvalidPattern
import at.petrak.hexcasting.client.getCenteredPattern
import com.defaulteugene.hexshield.utils.ImageUtil
import com.ibm.icu.text.DecimalFormat
import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.text.Text
import net.minecraft.util.Formatting
import net.minecraft.util.math.MathHelper
import net.minecraft.util.math.Vec2f
import java.awt.BasicStroke
import java.awt.Color
import java.awt.image.BufferedImage
import kotlin.math.floor
import kotlin.math.sqrt

@Environment(EnvType.CLIENT)
class HexPatternAdapter(private val pattern: HexPattern) : AbstractAdapter() {

    private lateinit var dots: List<Vec2f>
    private var name: String = ""
    private var mod: String = ""
    private var additionalData: String = ""
    private var additionalDataColor = Color.WHITE

    override fun preDraw() {
        super.preDraw()
        dots = getCenteredPattern(pattern, 1024f, 1024f, 16f).second
    }

    override fun draw() {
        val params = calculateOffsetAndScale()
        val offset = params.first
        val sc = params.second

        val first = dots[0].add(offset).multiply(sc).add(Vec2f(64f, 64f))
        val second = dots[1].add(offset).multiply(sc).add(Vec2f(64f, 64f))

        val lineLength = sqrt(first.distanceSquared(second).toDouble()).toFloat()
        val dotSize = MathHelper.clamp(Math.round(lineLength / 10), 4, 16)

        val stroke1 = BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND)
        val stroke2 = BasicStroke(dotSize / 3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND)

        var hue = 1f
        val step: Float = 0.9f / (dots.size - 1)

        graphics.color = Color(Color.HSBtoRGB(hue, 1f, 1f))
        graphics.fillOval(
            floor(first.x.toDouble()).toInt() - dotSize, floor(first.y.toDouble())
                .toInt() - dotSize, dotSize * 2, dotSize * 2
        )

        for (i in 0 until dots.size - 1) {
            graphics.color = Color(Color.HSBtoRGB(hue, 1f, 1f))

            val current = dots[i].add(offset).multiply(sc).add(Vec2f(64f, 64f))
            val next = dots[i + 1].add(offset).multiply(sc).add(Vec2f(64f, 64f))

            graphics.stroke = stroke2
            graphics.drawLine(
                floor(current.x.toDouble()).toInt(),
                floor(current.y.toDouble()).toInt(),
                floor(next.x.toDouble()).toInt(),
                floor(next.y.toDouble()).toInt()
            )

            ImageUtil.fillArrow(graphics, current, next, 0.3f)

            graphics.stroke = stroke1
            graphics.fillOval(
                floor(next.x.toDouble()).toInt() - dotSize / 2,
                floor(next.y.toDouble()).toInt() - dotSize / 2,
                dotSize,
                dotSize
            )
            graphics.fillOval(
                floor(current.x.toDouble()).toInt() - dotSize / 2, floor(current.y.toDouble())
                    .toInt() - dotSize / 2, dotSize, dotSize
            )

            hue -= step
        }
    }

    override fun postDraw() {
        prepareInfo()
        appendInfo(graphics, name, mod, additionalData, additionDataColor = additionalDataColor)
    }

    private fun calculateOffsetAndScale(): Pair<Vec2f, Float> {
        var minX = Int.MAX_VALUE.toFloat()
        var minY = Int.MAX_VALUE.toFloat()
        var maxX = Int.MIN_VALUE.toFloat()
        var maxY = Int.MIN_VALUE.toFloat()

        for (dot in dots) {
            if (dot.x > maxX) maxX = dot.x
            if (dot.x < minX) minX = dot.x
            if (dot.y > maxY) maxY = dot.y
            if (dot.y < minY) minY = dot.y
        }

        var offset = Vec2f(-minX, -minY)

        val w = maxX - minX
        val h = maxY - minY

        offset = if (w > h) {
            offset.add(Vec2f(0f, (w - h) / 2))
        } else {
            offset.add(Vec2f((h - w) / 2, 0f))
        }

        val sc = if ((w > h)) 896 / w else 896 / h

        return Pair(offset, sc)
    }

    private fun prepareInfo() {
        val signature = pattern.anglesSignature()

        when (signature) {
            "qqq" -> {
                name = Text.translatable("hexcasting.spell.hexcasting:open_paren").string
                mod = "hexcasting"
                additionalData = ""
            }

            "eee" -> {
                name = Text.translatable("hexcasting.spell.hexcasting:close_paren").string
                mod = "hexcasting"
                additionalData = ""
            }

            "qqqaw" -> {
                name = Text.translatable("hexcasting.spell.hexcasting:escape").string
                mod = "hexcasting"
                additionalData = ""
            }

            else -> {
                try {
                    prepareDefaultInfo()
                } catch (e: Exception) {
                    name = "???"
                    mod = ""
                    additionalData = ""
                }
            }
        }
    }

    @Throws(
        MishapInvalidPattern::class,
        NullPointerException::class,
        NoSuchMethodException::class,
        IllegalAccessException::class
    )
    private fun prepareDefaultInfo() {
        val pair = PatternRegistry.matchPatternAndID(pattern, null)
        name = pair.first.displayName.string
        mod = pair.second.namespace

        if ("hexcasting" != mod)
            return

        val identifier = pair.second.path.lowercase()
        additionalData = when (identifier) {
            "number" -> {
                val action = pair.first
                var info = ""
                if (action is ConstMediaAction) {
                    val field = action.javaClass.getDeclaredField("\$x")
                    field.isAccessible = true
                    info = DecimalFormat("####.####").format(field.getDouble(action))
                }
                info
            }

            "const/null" -> "Null"
            "const/true" -> "True"
            "const/false" -> "False"
            "const/vec/px" -> "(1, 0, 0)"
            "const/vec/py" -> "(0, 1, 0)"
            "const/vec/pz" -> "(0, 0, 1)"
            "const/vec/nx" -> "(-1, 0, 0)"
            "const/vec/ny" -> "(0, -1, 0)"
            "const/vec/nz" -> "(0, 0, -1)"
            "const/vec/0" -> "(0, 0, 0)"
            "const/double/pi" -> "π"
            "const/double/tau" -> "τ"
            "const/double/e" -> "e"
            "get_caster" -> "Caster"
            else -> ""
        }

        additionalDataColor = constantIotaColors.getOrDefault(identifier, Color.WHITE)
    }

    override fun createImage(): BufferedImage {
        preDraw()

        if (dots.size < 2)
            return GarbageAdapter.createImage()

        draw()
        postDraw()

        return image
    }

    companion object {
        private val constantIotaColors = HashMap<String, Color>()

        init {
            constantIotaColors["number"] = Color.BLUE
            constantIotaColors["const/null"] = Color.BLACK
            constantIotaColors["const/true"] = Color(Formatting.DARK_GREEN.colorValue!!)
            constantIotaColors["const/false"] = Color(Formatting.RED.colorValue!!)

            constantIotaColors["get_caster"] = Color(Formatting.DARK_RED.colorValue!!)

            for (vec in arrayOf(
                "const/vec/px",
                "const/vec/py",
                "const/vec/pz",
                "const/vec/nx",
                "const/vec/ny",
                "const/vec/nz",
                "const/vec/0"
            )) {
                constantIotaColors[vec] = Color.RED
            }

            for (c in arrayOf("const/double/pi", "const/double/tau", "const/double/e")) {
                constantIotaColors[c] = Color.MAGENTA
            }
        }
    }
}