package com.defaulteugene.hexshield.utils.iota_images

import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.text.Text
import java.awt.Color
import java.awt.Font

@Environment(EnvType.CLIENT)
object GarbageAdapter : AbstractAdapter() {

    override fun getFieldColor(): Color = Color.RED
    override fun getMainInfoColor(): Color = Color.RED
    override fun getAdditionalInfoColor(): Color = Color.RED

    override fun draw() {
        val prevFont = graphics.font

        graphics.font = Font(prevFont.fontName, prevFont.style, 768)
        graphics.color = getAdditionalInfoColor()
        val width = graphics.fontMetrics.stringWidth("X")
        graphics.drawString("X", 512 - width / 2, 1024 - 256)

        graphics.font = prevFont
    }

    override fun postDraw() {
        appendInfo(
            graphics,
            Text.translatable("hexcasting.spelldata.unknown").string,
            "hexcasting",
            "",
            getAdditionalInfoColor(),
            getAdditionalInfoColor()
        )
    }
}