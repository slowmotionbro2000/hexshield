package com.defaulteugene.hexshield.utils.iota_images

import at.petrak.hexcasting.api.spell.iota.IotaType
import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.util.Formatting
import java.awt.BasicStroke
import java.awt.Color

@Environment(EnvType.CLIENT)
class IotaIsNullAdapter(private val type : IotaType<*>) : AbstractAdapter() {

    override fun getFieldColor(): Color = Color(Formatting.DARK_RED.colorValue!!)
    override fun getAdditionalInfoColor(): Color = Color(Formatting.DARK_RED.colorValue!!)

    override fun draw() {
        val prevStroke = graphics.stroke

        graphics.stroke = BasicStroke(10f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND)
        graphics.color = getAdditionalInfoColor()
        graphics.drawOval(256, 256, 512, 512)
        graphics.drawLine(256, 1024-256, 1024-256, 256)

        graphics.stroke = prevStroke
    }

    override fun postDraw() {
        if (type == null) {
            // this case is unreachable, but...
            appendInfo(graphics, "???", "", "", additionDataColor = getAdditionalInfoColor())
        } else {
            appendInfo(graphics, type.typeName().string, "", "", additionDataColor = getAdditionalInfoColor())
        }
    }
}