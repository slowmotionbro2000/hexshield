package com.defaulteugene.hexshield.utils.iota_images

import at.petrak.hexcasting.api.spell.iota.IotaType
import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import java.awt.Color
import java.awt.Font

@Environment(EnvType.CLIENT)
class UnknownAdapter(private val type: IotaType<*>?) : AbstractAdapter() {

    override fun getAdditionalInfoColor(): Color = Color.MAGENTA

    override fun draw() {
        val prevFont = graphics.font

        graphics.font = Font(prevFont.fontName, prevFont.style, 768)
        graphics.color = Color.magenta
        val width = graphics.fontMetrics.stringWidth("?")
        graphics.drawString("?", 512 - width / 2, 1024 - 256)

        graphics.font = prevFont
    }

    override fun postDraw() {
        if (type == null) {
            // this case is unreachable, but...
            appendInfo(graphics, "???", "", "", additionDataColor = getAdditionalInfoColor())
        } else {
            appendInfo(graphics, type.typeName().string, "", "", additionDataColor = getAdditionalInfoColor())
        }
    }
}