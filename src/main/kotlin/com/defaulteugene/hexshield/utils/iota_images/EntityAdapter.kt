package com.defaulteugene.hexshield.utils.iota_images

import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.text.Text
import java.awt.Color

@Environment(EnvType.CLIENT)
object EntityAdapter : AbstractAdapter() {

    override fun getAdditionalInfoColor(): Color = Color.BLACK

    override fun draw() {
        val colorSkin = Color(185, 133, 107)
        val colorHair = Color(98, 70, 30)
        val colorJacket = Color(224, 151, 158)
        val colorJeans = Color(52, 52, 52)

        val headStartY = 128
        val bodyStartY = headStartY + 192
        val feetStartY = bodyStartY + 288

        val x0 = 320
        val x1 = x0 + 96

        val prevColor = graphics.color

        paintHead(colorSkin, colorHair, x1, headStartY)
        paintBody(colorJacket, x0, x1, bodyStartY)
        paintFeet(colorJeans, x1, feetStartY)

        graphics.color = prevColor
    }

    private fun paintHead(colorSkin: Color, colorHair: Color, x1: Int, headStartY: Int) {
        graphics.color = colorSkin
        graphics.fillRect(x1, headStartY + 48, 192, 192-48)

        graphics.color = colorHair
        graphics.fillRect(x1, headStartY, 192, 48)
        graphics.fillRect(x1, headStartY + 48, 48, 24)
        graphics.fillRect(x1, headStartY + 72, 24, 24)
        graphics.fillRect(x1 + 192 - 24, headStartY + 48, 24, 24)

        graphics.color = Color.WHITE
        graphics.fillRect(x1 + 24, headStartY + 96, 24, 48)
        graphics.fillRect(x1 + 192 - 48, headStartY + 96, 24, 48)

        graphics.color = colorHair
        graphics.fillRect(x1 + 48, headStartY + 96, 24, 48)
        graphics.fillRect(x1 + 192 - 72, headStartY + 96, 24, 48)

        graphics.color = Color.WHITE
        graphics.fillRect(x1 + 72, headStartY + 168, 48, 24)
    }

    private fun paintBody(colorJacket: Color, x0: Int, x1: Int, bodyStartY: Int) {
        graphics.color = colorJacket
        graphics.fillRect(x0, bodyStartY, 384, 288)

        graphics.color = Color.WHITE
        graphics.fillRect(x0, bodyStartY + 288 - 24, 96, 24)
        graphics.fillRect(x1 + 192, bodyStartY + 288 - 24, 96, 24)
    }

    private fun paintFeet(colorJeans: Color, x1: Int, feetStartY: Int) {
        graphics.color = colorJeans
        graphics.fillRect(x1, feetStartY, 192, 288)

        graphics.color = Color.LIGHT_GRAY
        graphics.fillRect(x1, feetStartY + 216, 192, 24)
        graphics.fillRect(x1, feetStartY + 216 + 48, 192, 24)
    }

    override fun postDraw() {
        appendInfo(
            graphics,
            Text.translatable("hexcasting.iota.hexcasting:entity").string,
            "hexcasting",
            "Entity",
            additionDataColor = getAdditionalInfoColor()
        )
    }
}