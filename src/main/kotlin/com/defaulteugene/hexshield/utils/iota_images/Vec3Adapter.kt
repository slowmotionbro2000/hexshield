package com.defaulteugene.hexshield.utils.iota_images

import com.defaulteugene.hexshield.utils.ImageUtil
import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.text.Text
import net.minecraft.util.math.Vec3d
import java.awt.BasicStroke
import java.awt.Color

@Environment(EnvType.CLIENT)
class Vec3Adapter(private val vec : Vec3d) : AbstractAdapter() {

    override fun getAdditionalInfoColor(): Color = Color.RED
    override fun draw() {
        val prevStroke = graphics.stroke

        graphics.stroke = BasicStroke(10f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND)
        ImageUtil.drawArrowLine(graphics, 64, 1024 - 256, 1024 - 64, 256, 64, 32)

        graphics.stroke = prevStroke
    }

    override fun postDraw() {
        appendInfo(
            graphics,
            Text.translatable("hexcasting.iota.hexcasting:vec3").string,
            "hexcasting",
            "(${vec.x}, ${vec.y}, ${vec.z})",
            additionDataColor = getAdditionalInfoColor()
        )
    }
}