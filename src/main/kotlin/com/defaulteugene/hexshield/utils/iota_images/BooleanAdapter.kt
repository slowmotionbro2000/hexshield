package com.defaulteugene.hexshield.utils.iota_images

import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.text.Text
import java.awt.Color
import java.awt.Font

@Environment(EnvType.CLIENT)
class BooleanAdapter private constructor(private val value : Boolean) : AbstractAdapter() {

    companion object {
        private val bTrue = BooleanAdapter(true)
        private val bFalse = BooleanAdapter(false)

        fun adapt(value: Boolean): BooleanAdapter
            = if (value) bTrue else bFalse
    }

    override fun getAdditionalInfoColor(): Color
        = if (value) Color.GREEN else Color.RED

    private fun signature(): String = if (value) "V" else "X"
    private fun getString(): String = if (value) "True" else "False"


    override fun draw() {
        val prevFont = graphics.font

        graphics.font = Font(prevFont.fontName, prevFont.style, 256)
        graphics.color = getAdditionalInfoColor()
        val width = graphics.fontMetrics.stringWidth(getString())
        graphics.drawString(getString(), 512 - width / 2, 512 + 64)

        graphics.font = Font(prevFont.fontName, prevFont.style, 64)
        val width2 = graphics.fontMetrics.stringWidth(signature())
        graphics.drawString(signature(), 512 - width2 / 2, 512 + 256)

        graphics.font = prevFont
    }

    override fun postDraw() {
        appendInfo(
            graphics,
            Text.translatable("hexcasting.iota.hexcasting:boolean").string,
            "hexcasting",
            "",
            additionDataColor = getAdditionalInfoColor()
        )
    }

}