package com.defaulteugene.hexshield.utils.iota_images

import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.text.Text
import java.awt.Color
import java.awt.Font

@Environment(EnvType.CLIENT)
class NumberAdapter(private val num: Double) : AbstractAdapter() {

    override fun getAdditionalInfoColor(): Color = Color.MAGENTA

    override fun draw() {
        val prevFont = graphics.font

        graphics.font = Font(prevFont.fontName, prevFont.style, 256)
        graphics.color = getAdditionalInfoColor()
        val num = String.format("%.2f", num)
        val width = graphics.fontMetrics.stringWidth(num)
        graphics.drawString(num, 512 - width / 2, 512 + 64)

        graphics.font = prevFont
    }

    override fun postDraw() {
        appendInfo(
            graphics,
            Text.translatable("hexcasting.iota.hexcasting:double").string,
            "hexcasting",
            "",
            additionDataColor = Color.BLUE
        )
    }
}