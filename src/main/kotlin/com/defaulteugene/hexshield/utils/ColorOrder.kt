package com.defaulteugene.hexshield.utils

import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.text.MutableText
import net.minecraft.text.TextColor
import net.minecraft.util.Formatting
import net.minecraft.util.math.MathHelper
import net.minecraft.world.World
import java.awt.Color

object ColorOrder {
    private val colors: List<Color> = listOf(
        Color.getHSBColor(120F / 360, 0F, 1F),
        Color.getHSBColor(220F / 360, 1F, 1F),
        Color.getHSBColor(320F / 360, 1F, 1F),
        Color.getHSBColor(60F / 360, 1F, 1F),
        Color.getHSBColor(160F / 360, 1F, 1F),
        Color.getHSBColor(280F / 360, 1F, 1F),
        Color.getHSBColor(20F / 360, 1F, 1F),
        Color.getHSBColor(200F / 360, 1F, 1F),
        Color.getHSBColor(0F / 360, 1F, 1F),
        Color.getHSBColor(180F / 360, 1F, 1F),
    )

    fun getColorByIndex(index: Int): Int {
        var adapted = index % colors.size
        if (adapted < 0)
            adapted += colors.size

        return colors[adapted].rgb
    }

    @Environment(EnvType.CLIENT)
    fun getTextColorByIndex(index: Int): TextColor {
        return TextColor.fromRgb(getColorByIndex(index))
    }

    fun setColorBrightness(parent: Color, brightness: Float): Color {
        val hsb = FloatArray(3)
        Color.RGBtoHSB(parent.red, parent.green, parent.blue, hsb)
        return Color.getHSBColor(hsb[0], hsb[1], brightness)
    }

    fun createRainbowText(text: MutableText, world: World?, shift: Int): MutableText {
        if (world == null)
            return text.styled { s -> s.withColor(Formatting.WHITE) }

        return text.styled { s -> s.withColor(TextColor.fromRgb(
            MathHelper.hsvToRgb(
                (world.time + shift) * 2 % 360 / 360f,
                1f,
                1f
            )
        ))}
    }
}