package com.defaulteugene.hexshield.utils

import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.predicate.NumberRange
import java.util.function.Supplier

class SimpleItemPredicate(val items: Set<Supplier<Item>>, private val amount: NumberRange.IntRange) {

    fun test(stack: ItemStack): Boolean {
        return !stack.isEmpty && testItem(stack.item) && amount.test(stack.count)
    }

    private fun testItem(item: Item): Boolean {
        items.forEach { entry -> run {
            if (entry.get() == item)
                return true
        }}
        return false
    }

}