package com.defaulteugene.hexshield.utils

import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.client.MinecraftClient
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

object FileUtil {

    @Environment(EnvType.CLIENT)
    fun saveImageToScreenshots(image: BufferedImage, name: String) {
        val dir = getOrCreateDirectoryForImage()
        ImageIO.write(image, "png", dir.resolve("$name.png"))
    }

    @Environment(EnvType.CLIENT)
    fun getOrCreateDirectoryForImage(): File {
        val dir = MinecraftClient.getInstance().runDirectory.resolve("screenshots").resolve("hexdump")
        dir.mkdirs()
        return dir
    }

}