package com.defaulteugene.hexshield.utils

import at.petrak.hexcasting.api.addldata.ADMediaHolder
import at.petrak.hexcasting.api.advancements.HexAdvancementTriggers
import at.petrak.hexcasting.api.block.circle.BlockEntityAbstractImpetus
import at.petrak.hexcasting.api.misc.HexDamageSources
import at.petrak.hexcasting.api.mod.HexConfig
import at.petrak.hexcasting.api.mod.HexStatistics
import at.petrak.hexcasting.api.spell.casting.CastingContext
import at.petrak.hexcasting.api.spell.casting.SpellCircleContext
import at.petrak.hexcasting.api.spell.mishaps.Mishap
import at.petrak.hexcasting.api.utils.compareMediaItem
import at.petrak.hexcasting.api.utils.extractMedia
import at.petrak.hexcasting.api.utils.isMediaItem
import at.petrak.hexcasting.xplat.IXplatAbstractions
import com.defaulteugene.hexshield.casting.mishaps.MishapCostRequirementUnsatisfied
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.server.network.ServerPlayerEntity
import java.util.function.Function
import kotlin.jvm.Throws
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.min

object MediaCastingUtil {

    private val flatMediaHolders : ArrayList<Function<PlayerEntity, List<ADMediaHolder>>> = ArrayList()

    @Throws(MishapCostRequirementUnsatisfied::class)
    fun assertEnoughMedia(context: CastingContext, media: Int) {
        if (context.source == CastingContext.CastSource.SPELL_CIRCLE && context.spellCircle != null) {
            assertCircleHaveMedia(context.caster, context.spellCircle!!, media)
        } else {
            assertPlayerHaveMedia(context.caster, media)
        }
    }

    @Throws(MishapCostRequirementUnsatisfied::class)
    fun assertPlayerHaveMedia(player: ServerPlayerEntity, media: Int) {
        if (withdrawMediaDirect(player, media, allowOvercast = true, simulation = true) > 0)
            throw MishapCostRequirementUnsatisfied(media)
    }

    @Throws(MishapCostRequirementUnsatisfied::class)
    fun assertCircleHaveMedia(player: ServerPlayerEntity, context: SpellCircleContext, media: Int) {
        val impetus = player.world.getBlockEntity(context.impetusPos)
        if (impetus !is BlockEntityAbstractImpetus)
            throw MishapCostRequirementUnsatisfied(media)

        if (impetus.media < media)
            throw MishapCostRequirementUnsatisfied(media)
    }

    /**
     * Withdraws media directly from player
     * @param player withdraw target
     * @param media how much media you want to draw
     * @param allowOvercast is casting from HP allowed
     * @param simulation if false then media won't be withdrawn, just check if we can withdraw
     * @return amount of media that can't be withdrawn
     */
    fun withdrawMediaDirect(player: ServerPlayerEntity, media: Int, allowOvercast: Boolean, simulation: Boolean): Int {
        if (player.isCreative)
            return 0

        if (media <= 0)
            return 0

        var costLeft = media
        val mediaSources = collectMediaHolders(player).sortedWith(Comparator(::compareMediaItem).reversed())

        for (mediaSource in mediaSources) {
            costLeft -= extractMedia(mediaSource, costLeft, simulate = simulation)
            if (costLeft <= 0)
                break
        }

        if (allowOvercast && costLeft > 0) {
            // Cast from HP!
            val mediaToHealth = HexConfig.common().mediaToHealthRate()
            val healthToRemove = max(costLeft.toDouble() / mediaToHealth, 0.5)
            val mediaAbleToCastFromHP = player.health * mediaToHealth

            val mediaToActuallyPayFor = min(mediaAbleToCastFromHP.toInt(), costLeft)

            costLeft -= if (!simulation) {
                Mishap.trulyHurt(player, HexDamageSources.OVERCAST, healthToRemove.toFloat())

                val actuallyTaken = ceil(mediaAbleToCastFromHP - (player.health * mediaToHealth)).toInt()

                HexAdvancementTriggers.OVERCAST_TRIGGER.trigger(player, actuallyTaken)
                player.increaseStat(HexStatistics.MEDIA_OVERCAST, media - costLeft)
                actuallyTaken
            } else {
                mediaToActuallyPayFor
            }
        }

        if (!simulation) {
            player.increaseStat(HexStatistics.MEDIA_USED, media - costLeft)
            HexAdvancementTriggers.SPEND_MEDIA_TRIGGER.trigger(
                player,
                media - costLeft,
                if (costLeft < 0) -costLeft else 0
            )
        }

        return costLeft
    }

    /**
     * Collects all media holders from player's inventory
     * @param player target player
     * @return list of media holders
     */
    fun collectMediaHolders(player: PlayerEntity): List<ADMediaHolder> {
        val result: MutableList<ADMediaHolder> = ArrayList()
        for (flatMediaHolder in flatMediaHolders) {
            result.addAll(flatMediaHolder.apply(player))
        }
        return result
    }

    init {
        flatMediaHolders.add {
            it.inventory.main
                .filter(::isMediaItem)
                .mapNotNull(IXplatAbstractions.INSTANCE::findMediaHolder)
        }
        flatMediaHolders.add {
            it.inventory.offHand
                .filter(::isMediaItem)
                .mapNotNull(IXplatAbstractions.INSTANCE::findMediaHolder)
        }
        flatMediaHolders.add {
            it.inventory.armor
                .filter(::isMediaItem)
                .mapNotNull(IXplatAbstractions.INSTANCE::findMediaHolder)
        }
    }
}