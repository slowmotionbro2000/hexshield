package com.defaulteugene.hexshield.utils

import com.ibm.icu.text.DecimalFormat

object MathUtil {

    val integerFormat = DecimalFormat("###,###")
    val floatingFormat = DecimalFormat("###,###.#")

    inline val Int.pretty get(): String = integerFormat.format(this)
    inline val Long.pretty get(): String = integerFormat.format(this)
    inline val Float.pretty get(): String = floatingFormat.format(this)
    inline val Double.pretty get(): String = floatingFormat.format(this)
    inline val Number.prettyI get(): String = integerFormat.format(this)
    inline val Number.prettyF get(): String = floatingFormat.format(this)
    inline val Number.pretty get(): String {
        if (this is Int)
            return this.prettyI
        return this.prettyF
    }

}