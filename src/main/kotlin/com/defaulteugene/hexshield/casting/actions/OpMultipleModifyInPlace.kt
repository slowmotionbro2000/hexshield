package com.defaulteugene.hexshield.casting.actions

import at.petrak.hexcasting.api.spell.*
import at.petrak.hexcasting.api.spell.casting.CastingContext
import at.petrak.hexcasting.api.spell.iota.Iota
import at.petrak.hexcasting.api.spell.iota.ListIota

object OpMultipleModifyInPlace : ConstMediaAction {
    override val argc = 3

    override fun execute(args: List<Iota>, ctx: CastingContext): List<Iota> {
        val list = args.getList(0, argc).toList()
        val index = args.getPositiveIntUnder(1, list.size, argc)
        val arg3 = args[2]

        val iotas: List<Iota> = if (arg3 is ListIota) arg3.list.toList() else listOf(arg3)

        val leftSide = list.subList(0, index)
        val rightSide = list.subList(index + 1, list.size)

        val result: ArrayList<Iota> = ArrayList()
        result.addAll(leftSide)
        result.addAll(iotas)
        result.addAll(rightSide)

        return result.asActionResult
    }
}