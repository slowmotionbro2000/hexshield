package com.defaulteugene.hexshield.casting.actions.sacrificialContract

import at.petrak.hexcasting.api.spell.RenderedSpell
import at.petrak.hexcasting.api.spell.casting.CastingContext
import com.defaulteugene.hexshield.Reference
import com.defaulteugene.hexshield.mixin.api.IPlayerEntity
import net.minecraft.text.Text
import net.minecraft.text.TextColor
import net.minecraft.util.Formatting

class DataToggleRenderedSpell(private val contractId: String) : RenderedSpell {
    override fun cast(ctx: CastingContext) {
        val beenActive = (ctx.caster as IPlayerEntity).isContractActive(contractId)
        (ctx.caster as IPlayerEntity).setContractActive(contractId, !beenActive)

        val text = Text.translatable(
            "%s.contract.%s.%s".format(
                Reference.MOD_ID,
                contractId,
                !beenActive
            )
        )

        text.style = text.style.withColor(if (beenActive) Formatting.RED else Formatting.GREEN)
        ctx.caster.sendMessage(text)
    }
}