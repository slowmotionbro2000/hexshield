package com.defaulteugene.hexshield.casting.actions

import at.petrak.hexcasting.api.misc.MediaConstants
import at.petrak.hexcasting.api.spell.ParticleSpray
import at.petrak.hexcasting.api.spell.RenderedSpell
import at.petrak.hexcasting.api.spell.SpellAction
import at.petrak.hexcasting.api.spell.casting.CastingContext
import at.petrak.hexcasting.api.spell.getItemEntity
import at.petrak.hexcasting.api.spell.iota.Iota
import at.petrak.hexcasting.api.spell.mishaps.MishapBadItem
import at.petrak.hexcasting.api.spell.mishaps.MishapBadOffhandItem
import com.defaulteugene.hexshield.utils.SimpleItemPredicate
import net.minecraft.entity.ItemEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.ItemStack
import net.minecraft.text.Text
import net.minecraft.util.Hand
import java.util.function.Supplier
import kotlin.jvm.Throws

class OpCreateCrafting(
    private val inHandPredicate: SimpleItemPredicate,
    private val wrongInHand: Supplier<Text>,
    private val inHandAmount: Int,
    private val inWorldPredicate: SimpleItemPredicate,
    private val wrongInWorld: Supplier<Text>,
    private val inWorldAmount: Int,
    private val craftingResult: Supplier<ItemStack>,
    private val craftingCost: Int = 100 * MediaConstants.DUST_UNIT
) : SpellAction {

    override val argc: Int = 1

    override fun execute(args: List<Iota>, ctx: CastingContext): Triple<RenderedSpell, Int, List<ParticleSpray>> {
        val entity = args.getItemEntity(0, argc)

        ctx.assertEntityInRange(entity)

        if (!inWorldPredicate.test(entity.stack) || entity.stack.count < inWorldAmount)
            throw MishapBadItem(entity, wrongInWorld.get())

        // just for assertion
        getItemFromHand(ctx.caster)

        return Triple(Spell(entity, this), craftingCost, listOf())
    }

    private data class Spell(val itemEntity: ItemEntity, val spell: OpCreateCrafting) : RenderedSpell {
        override fun cast(ctx: CastingContext) {
            if (!itemEntity.isAlive) return

            val gloopStack = spell.getItemFromHand(ctx.caster)

            val world = itemEntity.world
            val pos = itemEntity.pos

            if (itemEntity.stack.count > spell.inWorldAmount) {
                itemEntity.stack.count -= spell.inWorldAmount
            } else {
                itemEntity.kill()
            }

            gloopStack.count -= spell.inHandAmount

            val resultStack = spell.craftingResult.get()
            val entity = ItemEntity(world, pos.x, pos.y, pos.z, resultStack)
            world.spawnEntity(entity)
        }

    }

    @Throws(MishapBadOffhandItem::class)
    private fun getItemFromHand(player: PlayerEntity): ItemStack {
        val stackInOffHand = player.getStackInHand(Hand.OFF_HAND)
        if (inHandPredicate.test(stackInOffHand) && stackInOffHand.count >= inHandAmount)
            return stackInOffHand

        val stackInMainHand = player.getStackInHand(Hand.MAIN_HAND)
        if (inHandPredicate.test(stackInMainHand) && stackInMainHand.count >= inHandAmount)
            return stackInMainHand

        throw MishapBadOffhandItem(stackInOffHand, Hand.OFF_HAND, wrongInHand.get())
    }
}