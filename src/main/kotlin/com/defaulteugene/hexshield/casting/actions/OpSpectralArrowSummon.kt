package com.defaulteugene.hexshield.casting.actions

import at.petrak.hexcasting.api.spell.*
import at.petrak.hexcasting.api.spell.casting.CastingContext
import at.petrak.hexcasting.api.spell.iota.Iota
import at.petrak.hexcasting.api.spell.mishaps.MishapLocationTooFarAway
import com.defaulteugene.hexshield.mixin.api.IPersistentProjectile
import com.defaulteugene.hexshield.registry.Items
import net.minecraft.entity.projectile.PersistentProjectileEntity
import net.minecraft.item.ItemStack
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.util.math.Vec3d

object OpSpectralArrowSummon : SpellAction {
    override val argc = 3

    override fun execute(args: List<Iota>, ctx: CastingContext): Triple<RenderedSpell, Int, List<ParticleSpray>> {
        val spawnPos = args.getVec3(0, argc)
        val motion = args.getVec3(1, argc)
        val critical = args.getBool(2, argc)
        ctx.assertVecInRange(spawnPos)

        val motionLen = motion.length()
        val cost: Long = (500 + 100 * motionLen * motionLen * motionLen * (if (critical) 2 else 1)).toLong()

        try {
            return Triple(
                Spell(spawnPos, motion, critical, ctx.caster),
                Math.toIntExact(cost),
                listOf()
            )
        } catch (e: ArithmeticException) {
            throw MishapLocationTooFarAway(motion)
        }
    }

    private data class Spell(val spawnPos: Vec3d, val motion: Vec3d, val critical: Boolean, val caster: ServerPlayerEntity): RenderedSpell {
        override fun cast(ctx: CastingContext) {
            val arrow = Items.shadowArrow

            val arrowEntity = arrow.createArrow(caster.world, ItemStack(arrow), caster)
            arrowEntity.setPosition(spawnPos)
            arrowEntity.velocity = motion
            arrowEntity.pickupType = PersistentProjectileEntity.PickupPermission.CREATIVE_ONLY
            arrowEntity.isCritical = critical

            if (arrowEntity is IPersistentProjectile) {
                arrowEntity.life = 1000
            }

            caster.world.spawnEntity(arrowEntity)
        }
    }
}