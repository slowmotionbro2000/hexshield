package com.defaulteugene.hexshield.casting.actions

import at.petrak.hexcasting.api.misc.MediaConstants
import at.petrak.hexcasting.api.spell.*
import at.petrak.hexcasting.api.spell.casting.CastingContext
import at.petrak.hexcasting.api.spell.iota.Iota
import at.petrak.hexcasting.api.spell.mishaps.MishapBadItem
import at.petrak.hexcasting.common.lib.HexItems
import com.defaulteugene.hexshield.casting.mishaps.MishapNumberOutOfBounds
import com.defaulteugene.hexshield.registry.Items
import com.defaulteugene.hexshield.utils.MediaCastingUtil
import net.minecraft.entity.ItemEntity
import net.minecraft.item.ItemStack
import kotlin.math.pow

object OpCreatePhilosopherBattery : SpellAction {
    override val argc: Int = 2

    override fun execute(args: List<Iota>, ctx: CastingContext): Triple<RenderedSpell, Int, List<ParticleSpray>> {
        val entity = args.getItemEntity(0, argc)
        val media = args.getDouble(1, argc)

        if (media <= 0 || media > 20)
            throw MishapNumberOutOfBounds(media, 0, 20)

        ctx.assertEntityInRange(entity)

        val mediaForCost = 5 + media / 20 * 15 // turn [0, 20] to [5, 20]

        val costDust = mediaForCost.pow(4)
        val spellCost = (costDust * MediaConstants.DUST_UNIT).toInt()

        MediaCastingUtil.assertEnoughMedia(ctx, spellCost)

        if (!entity.stack.isOf(HexItems.BATTERY))
            throw MishapBadItem(entity, ItemStack(HexItems.BATTERY).name)

        if (entity.stack.count != 1)
            throw MishapBadItem.of(entity, "only_one")

        return Triple(Spell(entity, (media * MediaConstants.DUST_UNIT).toInt()), spellCost, listOf())
    }

    private data class Spell(val itemEntity: ItemEntity, val media: Int) : RenderedSpell {
        override fun cast(ctx: CastingContext) {
            if (!itemEntity.isAlive) return

            val world = itemEntity.world
            val pos = itemEntity.pos

            itemEntity.kill()

            val resultStack = Items.itemPhilosopherMediaBattery.newStack(media)
            val entity = ItemEntity(world, pos.x, pos.y, pos.z, resultStack)
            world.spawnEntity(entity)
        }

    }
}