package com.defaulteugene.hexshield.casting.actions

import at.petrak.hexcasting.api.spell.ParticleSpray
import at.petrak.hexcasting.api.spell.RenderedSpell
import at.petrak.hexcasting.api.spell.SpellAction
import at.petrak.hexcasting.api.spell.casting.CastingContext
import at.petrak.hexcasting.api.spell.getEntity
import at.petrak.hexcasting.api.spell.iota.Iota
import net.minecraft.entity.Entity
import net.minecraft.particle.ParticleTypes
import net.minecraft.server.world.ServerWorld

object OpHeartWhisper : SpellAction {
    override val argc = 1

    override fun execute(args: List<Iota>, ctx: CastingContext): Triple<RenderedSpell, Int, List<ParticleSpray>> {
        val entity = args.getEntity(0, argc)
        ctx.assertEntityInRange(entity)

        return Triple(
            Spell(entity),
            0,
            listOf()
        )
    }

    private data class Spell(val target: Entity): RenderedSpell {
        override fun cast(ctx: CastingContext) {
            val world = target.world
            val rand = world.random

            if (world !is ServerWorld)
                return

            world.spawnParticles(
                ParticleTypes.HEART,
                target.x, target.y + 1, target.z,
                4 + rand.nextInt(4),
                rand.nextDouble(), rand.nextDouble() / 2, rand.nextDouble(),
                .0
            )
        }
    }
}