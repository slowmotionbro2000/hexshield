package com.defaulteugene.hexshield.casting.actions.sacrificialContract

import at.petrak.hexcasting.api.spell.ParticleSpray
import at.petrak.hexcasting.api.spell.RenderedSpell
import at.petrak.hexcasting.api.spell.SpellAction
import at.petrak.hexcasting.api.spell.casting.CastingContext
import at.petrak.hexcasting.api.spell.iota.Iota

class OpSacrificialContract(private val data : String, private val castingCost: Int) : SpellAction {

    override val argc: Int = 0

    override fun execute(args: List<Iota>, ctx: CastingContext): Triple<RenderedSpell, Int, List<ParticleSpray>>
        = Triple(DataToggleRenderedSpell(data), castingCost, listOf())
}