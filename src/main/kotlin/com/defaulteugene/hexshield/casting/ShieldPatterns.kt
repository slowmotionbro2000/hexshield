package com.defaulteugene.hexshield.casting

import at.petrak.hexcasting.api.PatternRegistry
import at.petrak.hexcasting.api.misc.MediaConstants
import at.petrak.hexcasting.api.spell.math.HexDir
import at.petrak.hexcasting.api.spell.math.HexPattern
import com.defaulteugene.hexshield.Reference
import com.defaulteugene.hexshield.casting.actions.*
import com.defaulteugene.hexshield.casting.actions.sacrificialContract.OpSacrificialContract
import com.defaulteugene.hexshield.entity.data.PlayerDataSacrificialContract
import com.defaulteugene.hexshield.registry.Items
import com.defaulteugene.hexshield.utils.SimpleItemPredicate
import com.samsthenerd.hexgloop.items.HexGloopItems
import net.minecraft.item.ItemStack
import net.minecraft.predicate.NumberRange
import net.minecraft.util.Identifier
import java.util.function.Supplier

object ShieldPatterns {

    fun registerPatterns() {
        PatternRegistry.mapPattern(
            HexPattern.fromAngles("eeewwwdwwweee", HexDir.NORTH_EAST),
            Identifier(Reference.MOD_ID, "heartwhisper"),
            OpHeartWhisper
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("wdewewedwewwadeeed", HexDir.NORTH_WEST),
            Identifier(Reference.MOD_ID, "spectralarrow"),
            OpSpectralArrowSummon
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("qdewedqwwwwwwawwwwwawwwwwqaqqa", HexDir.NORTH_WEST),
            Identifier(Reference.MOD_ID, "philosopher_battery"),
            OpCreatePhilosopherBattery
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("wewewdeqadawwwwadaqedwewewwwqwwdewqawdwwdwaqwedwwqw", HexDir.NORTH_WEST),
            Identifier(Reference.MOD_ID, "craft_wardenyst"),
            OpCreateCrafting(
                SimpleItemPredicate(setOf(Supplier{ HexGloopItems.GLOOP_ITEM.get() }), NumberRange.IntRange.atLeast(8)),
                { ItemStack(HexGloopItems.GLOOP_ITEM.get()).toHoverableText().copy().append("x8") },
                8,
                SimpleItemPredicate(setOf(Supplier{ Items.wardenSoul }), NumberRange.IntRange.atLeast(1)),
                { ItemStack(Items.wardenSoul).toHoverableText() },
                1,
                { ItemStack(Items.wardenShard) }
            )
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("dqwqdweqeeaedadadeaeeqeedqaeeaqd", HexDir.NORTH_EAST),
            Identifier(Reference.MOD_ID, "craft_dragonyst"),
            OpCreateCrafting(
                SimpleItemPredicate(setOf(Supplier{ Items.wardenShard }), NumberRange.IntRange.atLeast(8)),
                { ItemStack(Items.wardenShard).toHoverableText().copy().append("x8") },
                8,
                SimpleItemPredicate(setOf(Supplier{ Items.dragonSoul }), NumberRange.IntRange.atLeast(1)),
                { ItemStack(Items.dragonSoul).toHoverableText() },
                1,
                { ItemStack(Items.dragonShard) },
                craftingCost = 1000 * MediaConstants.DUST_UNIT
            )
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("wedqdew", HexDir.SOUTH_WEST),
            Identifier(Reference.MOD_ID, "multiple_modify_in_place"),
            OpMultipleModifyInPlace
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("wwwqwqwqwqwww", HexDir.SOUTH_WEST),
            Identifier(Reference.MOD_ID, "vampire_contract"),
            OpSacrificialContract(PlayerDataSacrificialContract.VAMPIRE_CONTRACT, 2000_0000)
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("qqqqqawwdwewewewewewqawwqwwqwwqwwqwwqww", HexDir.WEST),
            Identifier(Reference.MOD_ID, "epiphany_contract"),
            OpSacrificialContract(PlayerDataSacrificialContract.EPIPHANY_CONTRACT, 5000_0000)
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("wwweqwdwwwwdwqewwdwweqdwdqedeqaqeadaadaeqdaadadadaadqeadadad", HexDir.NORTH_WEST),
            Identifier(Reference.MOD_ID, "resistance_contract"),
            OpSacrificialContract(PlayerDataSacrificialContract.RESISTANCE_CONTRACT, 2000_0000)
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("qaqdewwqwaqdadadawwwwadadadqawqwwe", HexDir.NORTH_EAST),
            Identifier(Reference.MOD_ID, "everwings_contract"),
            OpSacrificialContract(PlayerDataSacrificialContract.EVERWINGS_CONTRACT, 2000_0000)
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("wwwwwwaqwwwwwdwqawwewqawdwqawwade", HexDir.NORTH_EAST),
            Identifier(Reference.MOD_ID, "cursed_contract"),
            OpSacrificialContract(PlayerDataSacrificialContract.CURSED_CONTRACT, 2000_0000)
        )

        PatternRegistry.mapPattern(
            HexPattern.fromAngles("awawwawwwawwwwawwwwwawwwwwwawwwwww", HexDir.SOUTH_WEST),
            Identifier(Reference.MOD_ID, "range_contract"),
            OpSacrificialContract(PlayerDataSacrificialContract.RANGE_CONTRACT, 5000_0000)
        )
    }

}