package com.defaulteugene.hexshield.casting.mishaps

import at.petrak.hexcasting.api.misc.FrozenColorizer
import at.petrak.hexcasting.api.spell.casting.CastingContext
import at.petrak.hexcasting.api.spell.iota.GarbageIota
import at.petrak.hexcasting.api.spell.iota.Iota
import at.petrak.hexcasting.api.spell.mishaps.Mishap
import at.petrak.hexcasting.api.utils.asTranslatedComponent
import com.defaulteugene.hexshield.utils.MathUtil.pretty
import net.minecraft.text.Text
import net.minecraft.util.DyeColor
import net.minecraft.util.Formatting

class MishapNumberOutOfBounds(private val number: Number, private val minimum: Number, private val maximum: Number)
    : Mishap() {
    override fun accentColor(ctx: CastingContext, errorCtx: Context): FrozenColorizer
        = dyeColor(DyeColor.BLUE)

    override fun errorMessage(ctx: CastingContext, errorCtx: Context): Text
        = "hexshield.mishap.number_out_of_bound".asTranslatedComponent(
            Text.literal(number.pretty).styled { s -> s.withColor(Formatting.YELLOW) },
            Text.literal(minimum.pretty).styled { s -> s.withColor(Formatting.YELLOW) },
            Text.literal(maximum.pretty).styled { s -> s.withColor(Formatting.YELLOW) }
        ).styled { s -> s.withColor(Formatting.RED) }

    override fun execute(ctx: CastingContext, errorCtx: Context, stack: MutableList<Iota>) {
        stack.add(GarbageIota())
    }
}