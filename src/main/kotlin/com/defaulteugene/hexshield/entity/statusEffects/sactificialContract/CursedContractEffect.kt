package com.defaulteugene.hexshield.entity.statusEffects.sactificialContract

import com.defaulteugene.hexshield.item.ICustomDisplayInfo
import net.minecraft.entity.effect.StatusEffect
import net.minecraft.entity.effect.StatusEffectCategory
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.text.Text

object CursedContractEffect : StatusEffect(StatusEffectCategory.NEUTRAL, 0), ICustomDisplayInfo {

    override fun getDisplayLabel(effect: StatusEffectInstance): String? {
        return Text.translatable("hexshield.misc.literal.inf").string
    }
}