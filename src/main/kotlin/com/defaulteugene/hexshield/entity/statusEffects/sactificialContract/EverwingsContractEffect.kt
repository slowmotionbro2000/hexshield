package com.defaulteugene.hexshield.entity.statusEffects.sactificialContract

import com.defaulteugene.hexshield.item.ICustomDisplayInfo
import com.defaulteugene.hexshield.mixin.api.IPlayerEntity
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.attribute.AttributeContainer
import net.minecraft.entity.effect.StatusEffect
import net.minecraft.entity.effect.StatusEffectCategory
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.entity.effect.StatusEffects
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text

object EverwingsContractEffect : StatusEffect(StatusEffectCategory.NEUTRAL, 0), ICustomDisplayInfo {

    override fun getDisplayLabel(effect: StatusEffectInstance): String? {
        return Text.translatable("hexshield.misc.literal.inf").string
    }

    override fun applyUpdateEffect(entity: LivingEntity?, amplifier: Int) {
        if (entity !is ServerPlayerEntity) return
        if (entity.entityWorld.isClient) return
        if (entity.isCreative || entity.isSpectator) return

        if (!(entity as IPlayerEntity).tickEverwingsContractCounter(entity.abilities.flying)) {
            entity.removeStatusEffect(this)
            return
        }

        if (!entity.abilities.allowFlying) {
            entity.abilities.allowFlying = true
            entity.sendAbilitiesUpdate()
        }
    }

    override fun onRemoved(entity: LivingEntity?, attributes: AttributeContainer?, amplifier: Int) {
        if (entity !is PlayerEntity) return
        if (entity.isCreative || entity.isSpectator) return
        if (entity.hasStatusEffect(this)) return

        entity.abilities.allowFlying = false
        entity.abilities.flying = false
        entity.sendAbilitiesUpdate()

        entity.addStatusEffect(StatusEffectInstance(StatusEffects.SLOW_FALLING, 200))
    }

    override fun canApplyUpdateEffect(duration: Int, amplifier: Int): Boolean
        = true

}