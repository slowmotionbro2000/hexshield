package com.defaulteugene.hexshield.entity.statusEffects.sactificialContract

import com.defaulteugene.hexshield.item.ICustomDisplayInfo
import net.minecraft.entity.attribute.EntityAttributeModifier
import net.minecraft.entity.attribute.EntityAttributes
import net.minecraft.entity.effect.StatusEffect
import net.minecraft.entity.effect.StatusEffectCategory
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.text.Text

object VampireContractEffect : StatusEffect(StatusEffectCategory.NEUTRAL, 0), ICustomDisplayInfo {

    override fun getDisplayLabel(effect: StatusEffectInstance): String? {
        return Text.translatable("hexshield.misc.literal.inf").string
    }

    init {
        addAttributeModifier(EntityAttributes.GENERIC_ATTACK_DAMAGE, "648D7064-6A60-4F59-8ABC-C2C23A6DD7A3", 1.5, EntityAttributeModifier.Operation.MULTIPLY_BASE)
    }
}