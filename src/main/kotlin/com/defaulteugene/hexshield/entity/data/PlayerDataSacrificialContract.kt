package com.defaulteugene.hexshield.entity.data

import com.defaulteugene.hexshield.mixin.api.IPlayerEntity
import com.defaulteugene.hexshield.registry.PotionEffects
import com.google.common.collect.ImmutableMap
import net.minecraft.entity.effect.StatusEffect
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.entity.player.PlayerEntity

object PlayerDataSacrificialContract {

    const val VAMPIRE_CONTRACT = "vampire"
    const val EPIPHANY_CONTRACT = "epiphany"
    const val RESISTANCE_CONTRACT = "resistance"
    const val EVERWINGS_CONTRACT = "everwings"
    const val CURSED_CONTRACT = "cursed"
    const val RANGE_CONTRACT = "range"

    val mapContractToEffect : ImmutableMap<String, StatusEffect>

    fun applyContracts(player: PlayerEntity) {
        mapContractToEffect.forEach { (name, effect) ->
            run {
                if ((player as IPlayerEntity).isContractActive(name)) {
                    player.addStatusEffect(StatusEffectInstance(effect, 400, 0, true, false, true))
                }
            }
        }

    }

    init {
        val effectsBuilder = ImmutableMap.Builder<String, StatusEffect>()
        effectsBuilder.put(VAMPIRE_CONTRACT, PotionEffects.effectVampireContract)
        effectsBuilder.put(EPIPHANY_CONTRACT, PotionEffects.effectEpiphanyContract)
        effectsBuilder.put(RESISTANCE_CONTRACT, PotionEffects.effectResistanceContract)
        effectsBuilder.put(EVERWINGS_CONTRACT, PotionEffects.effectEverwingsContract)
        effectsBuilder.put(CURSED_CONTRACT, PotionEffects.effectCursedContract)
        effectsBuilder.put(RANGE_CONTRACT, PotionEffects.effectRangeContract)
        mapContractToEffect = effectsBuilder.build()
    }
}