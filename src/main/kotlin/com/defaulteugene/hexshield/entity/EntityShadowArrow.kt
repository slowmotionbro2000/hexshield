package com.defaulteugene.hexshield.entity

import com.defaulteugene.hexshield.mixin.api.ISoulDropEntity
import com.defaulteugene.hexshield.registry.Entities
import com.defaulteugene.hexshield.registry.Items
import net.minecraft.entity.EntityType
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.entity.effect.StatusEffects
import net.minecraft.entity.projectile.PersistentProjectileEntity
import net.minecraft.item.ItemStack
import net.minecraft.util.hit.EntityHitResult
import net.minecraft.world.World

class EntityShadowArrow: PersistentProjectileEntity {

    constructor(entityType: EntityType<out EntityShadowArrow>?, world: World?): super(entityType, world)
    constructor(world: World?, owner: LivingEntity?): super(Entities.shadowArrow, owner, world)
    constructor(world: World?, x: Double, y: Double, z: Double): super(Entities.shadowArrow, x, y, z, world)

    override fun asItemStack(): ItemStack {
        return ItemStack(Items.shadowArrow)
    }

    override fun onEntityHit(entityHitResult: EntityHitResult?) {
        super.onEntityHit(entityHitResult)
        if (entityHitResult == null)
            return

        if (entityHitResult.entity is ISoulDropEntity) {
            (entityHitResult.entity as ISoulDropEntity).dropSoul()
        }
    }

    override fun onHit(target: LivingEntity?) {
        super.onHit(target)
        if (target == null)
            return

        val seInstance = StatusEffectInstance(StatusEffects.GLOWING, 200, 3)
        target.addStatusEffect(seInstance)
    }

}