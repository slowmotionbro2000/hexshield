package com.defaulteugene.hexshield.command

import com.mojang.brigadier.CommandDispatcher
import com.mojang.brigadier.LiteralMessage
import com.mojang.brigadier.Message
import com.mojang.brigadier.builder.LiteralArgumentBuilder
import com.mojang.brigadier.exceptions.CommandSyntaxException
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType
import net.minecraft.server.command.CommandManager
import net.minecraft.server.command.ServerCommandSource

abstract class AbstractCommand {
    abstract fun getName(): String
    abstract fun getPermissionLevel(): Int
    abstract fun getDedicationType(): EnumDedicationType
    open fun registerCommandArguments(builder: LiteralArgumentBuilder<ServerCommandSource>): LiteralArgumentBuilder<ServerCommandSource> {
        return builder
    }

    fun register(dispatcher: CommandDispatcher<ServerCommandSource>, dedicated: Boolean) {
        dispatcher.register(registerCommandArguments(
            CommandManager.literal(getName())
                .requires { source -> canRun(dedicated) && source.hasPermissionLevel(getPermissionLevel()) }
        ))
    }

    private fun canRun(dedicated: Boolean): Boolean {
        return when (getDedicationType()) {
            EnumDedicationType.ANY -> true
            EnumDedicationType.DEDICATED -> dedicated
            EnumDedicationType.NON_DEDICATED -> !dedicated
        }
    }

    fun createException(msg: String): CommandSyntaxException {
        val message: Message = LiteralMessage(msg)
        return CommandSyntaxException(DynamicCommandExceptionType { _ -> message }, message)
    }

    enum class EnumDedicationType {
        ANY,
        DEDICATED,
        NON_DEDICATED
    }
}