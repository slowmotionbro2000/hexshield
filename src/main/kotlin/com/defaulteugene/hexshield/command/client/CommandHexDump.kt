package com.defaulteugene.hexshield.command.client

import at.petrak.hexcasting.api.item.HexHolderItem
import at.petrak.hexcasting.api.item.IotaHolderItem
import at.petrak.hexcasting.api.spell.iota.EntityIota
import at.petrak.hexcasting.api.spell.iota.ListIota
import at.petrak.hexcasting.api.utils.getList
import at.petrak.hexcasting.common.items.magic.ItemPackagedHex
import at.petrak.hexcasting.common.lib.hex.HexIotaTypes
import com.defaulteugene.hexshield.utils.FileUtil
import com.defaulteugene.hexshield.utils.ImageUtil
import com.defaulteugene.hexshield.utils.ImageUtil.IotaWrapper
import com.mojang.brigadier.arguments.StringArgumentType
import com.mojang.brigadier.builder.LiteralArgumentBuilder
import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.fabricmc.fabric.api.client.command.v2.ClientCommandManager
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource
import net.minecraft.client.MinecraftClient
import net.minecraft.client.gui.hud.MessageIndicator
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NbtCompound
import net.minecraft.text.ClickEvent
import net.minecraft.text.Text
import net.minecraft.util.Formatting
import net.minecraft.util.Hand
import java.awt.Color
import java.text.SimpleDateFormat
import java.util.*

@Environment(EnvType.CLIENT)
object CommandHexDump {

    private val indicator = MessageIndicator(Color.MAGENTA.rgb, null, null, "HexShield Dumper")
    private val indicatorError = MessageIndicator(Color.RED.rgb, null, null, "HexShield Dumper")

    private fun execute(filename: String?): Int {
        val player = MinecraftClient.getInstance().player ?: return -1

        var stack = player.getStackInHand(Hand.MAIN_HAND)

        var hex = getIotas(stack)
        if (hex == null)
            stack = player.getStackInHand(Hand.OFF_HAND)

        hex = getIotas(stack)
        if (hex.isNullOrEmpty()) {
            error(
                Text.translatable("hexshield.misc.hexdump.iota_not_found").styled { s -> s.withColor(Formatting.RED) })
            return -1
        }

        val images = hex.map { pattern ->
            ImageUtil.iotaToImage(pattern)
        }

        val nameAlias = filename ?: if (stack.hasCustomName()) stack.name.string else "UNKNOWN"

        val result = ImageUtil.withCaption(
            ImageUtil.combineImages(4, images.toTypedArray()),
            filename ?: if (stack.hasCustomName()) stack.name.string else null
        )

        val name = "$nameAlias ${SimpleDateFormat("yyyy-MM-dd_HH.mm.ss").format(Calendar.getInstance().time)}"

        FileUtil.saveImageToScreenshots(result, name)
        val dirPath = FileUtil.getOrCreateDirectoryForImage()
        val filePath = dirPath.resolve("$name.png")

        val base = Text.translatable("hexshield.misc.hexdump.success").styled { s -> s.withColor(Formatting.GREEN) }
        val file = Text.literal("$name.png").styled { s ->
            s.withClickEvent(ClickEvent(ClickEvent.Action.OPEN_FILE, filePath.toString()))
                .withUnderline(true)
        }
        val dir = Text.translatable("hexshield.misc.hexdump.directory").styled { s ->
            s.withClickEvent(ClickEvent(ClickEvent.Action.OPEN_FILE, dirPath.toString()))
                .withColor(Formatting.GRAY)
                .withItalic(true)
        }

        message(base.append(file))
        message(dir)

        return 0
    }

    private fun message(text: Text, ind: MessageIndicator = indicator) {
        MinecraftClient.getInstance().inGameHud.chatHud.addMessage(text, null, ind)
    }

    private fun error(text: Text) {
        message(text, indicatorError)
    }

    private fun getIotas(stack: ItemStack): List<IotaWrapper>? {
        if (stack.item is HexHolderItem)
            return getIotasFromHexHolder(stack, stack.item as HexHolderItem)

        if (stack.item is IotaHolderItem)
            return getIotasFromIotaHolder(stack, stack.item as IotaHolderItem)

        return null
    }

    private fun getIotasFromHexHolder(stack: ItemStack, parent: HexHolderItem): List<IotaWrapper> {
        // overwrite ItemPackagedHex logic to manually prevent IllegalArgumentException from level=null
        // in global EACH item MUST use ItemPackagedHex, not HexHolderItem directly
        if (parent is ItemPackagedHex) {
            val patsTag = stack.nbt?.getList(ItemPackagedHex.TAG_PROGRAM, NbtCompound.COMPOUND_TYPE) ?: return listOf()

            val result = ArrayList<IotaWrapper>()
            patsTag.forEach { tag ->
                run {
                    if (tag is NbtCompound)
                        getIotasFromIotaHolderTagged(tag).let { result.addAll(it) }
                }
            }

            return result
        }

        // if something implements HexHolderItem as-is then... Let's just try =\
        return try {
            val hex = parent.getHex(stack, null)

            hex?.flatMap { h ->
                if (h is ListIota)
                    return@flatMap unwrapList(h)
                listOf(IotaWrapper(h, h.type))
            } ?: listOf()
        } catch (e: Exception) {
            listOf()
        }
    }

    private fun getIotasFromIotaHolder(stack: ItemStack, parent: IotaHolderItem): List<IotaWrapper> {
        val tag = parent.readIotaTag(stack) ?: return listOf()
        return getIotasFromIotaHolderTagged(tag)
    }

    /**
     * overload required for:
     * EntityIota
     * ListIota
     * Non-HexCasting iota
     */
    private fun getIotasFromIotaHolderTagged(tag: NbtCompound): List<IotaWrapper> {
        val type = HexIotaTypes.getTypeFromTag(tag) ?: return listOf()
        val data = tag.get(HexIotaTypes.KEY_DATA) ?: return listOf()

        // EntityIota is non-deserializable on client side
        if (type == EntityIota.TYPE) {
            return listOf(IotaWrapper(null, EntityIota.TYPE))
        }

        if (type == ListIota.TYPE) {
            return unwrapList(ListIota.TYPE.deserialize(data, null) ?: return listOf(), needBraces = false)
        }

        return try {
            val result = type.deserialize(data, null)
            listOf(IotaWrapper(result, type))
        } catch (iae: IllegalArgumentException) {
            listOf(IotaWrapper(null, type))
        }
    }

    private fun unwrapList(list: ListIota, needBraces: Boolean = true): List<IotaWrapper> {
        val inner = list.list.flatMap { iota ->
            if (iota is ListIota) {
                return@flatMap unwrapList(iota)
            }
            listOf(IotaWrapper(iota, iota.type))
        }

        if (!needBraces)
            return inner

        return listOf(listOf(IotaWrapper.OPEN_BRACE), inner, listOf(IotaWrapper.CLOSE_BRACE)).flatten()
    }


    fun getBuilder(): LiteralArgumentBuilder<FabricClientCommandSource> {
        return ClientCommandManager.literal("hexdump")
            .then(
                ClientCommandManager.argument("filename", StringArgumentType.string())
                    .executes { context -> execute(context.getArgument("filename", String::class.java)) }
            )
            .executes { _ -> execute(null) }

    }
}