package com.defaulteugene.hexshield.item

import com.samsthenerd.hexgloop.items.ItemSimpleMediaProvider
import net.minecraft.item.ItemStack
import net.minecraft.util.Rarity

class ItemSimpleMediaProvider(settings: Settings, mediaAmt: Int, priority: Int, private val rarity: Rarity, private val enchanted: Boolean = false)
    : ItemSimpleMediaProvider(settings, mediaAmt, priority) {

    override fun getRarity(stack: ItemStack?): Rarity = rarity

    override fun hasGlint(stack: ItemStack?): Boolean {
        return enchanted || super.hasGlint(stack)
    }
}