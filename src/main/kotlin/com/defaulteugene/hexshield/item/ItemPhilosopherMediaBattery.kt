package com.defaulteugene.hexshield.item

import at.petrak.hexcasting.api.misc.MediaConstants
import at.petrak.hexcasting.common.items.magic.ItemMediaHolder
import com.defaulteugene.hexshield.HexShield
import com.defaulteugene.hexshield.utils.ColorOrder
import net.minecraft.client.item.TooltipContext
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemStack
import net.minecraft.text.Text
import net.minecraft.util.Rarity
import net.minecraft.util.collection.DefaultedList
import net.minecraft.world.World
import java.text.DecimalFormat

class ItemPhilosopherMediaBattery(settings: Settings) : ItemMediaHolder(settings), ICustomDisplayInfo {

    fun newStack(media: Int): ItemStack {
        if (media <= 0)
            return ItemStack(this)

        return withMedia(ItemStack(this), media, media)
    }

    override fun canProvideMedia(stack: ItemStack?): Boolean {
        return true
    }

    override fun canRecharge(stack: ItemStack?): Boolean {
        return false
    }

    override fun getMedia(stack: ItemStack?): Int {
        return getMaxMedia(stack)
    }

    override fun setMedia(stack: ItemStack?, media: Int) {
    }

    override fun isItemBarVisible(pStack: ItemStack?): Boolean {
        return false
    }

    override fun appendStacks(group: ItemGroup?, stacks: DefaultedList<ItemStack>?) {
        if (!isIn(group)) return
        if (!HexShield.isDevEnvironment) return

        val mediaAmounts = listOf(
            MediaConstants.DUST_UNIT / 100,
            MediaConstants.DUST_UNIT / 10,
            MediaConstants.DUST_UNIT,
            5 * MediaConstants.DUST_UNIT,
            10 * MediaConstants.DUST_UNIT,
            15 * MediaConstants.DUST_UNIT,
            20 * MediaConstants.DUST_UNIT
        )

        mediaAmounts.forEach { media -> stacks?.add(newStack(media)) }
    }

    override fun appendTooltip(
        pStack: ItemStack?,
        pLevel: World?,
        pTooltipComponents: MutableList<Text>?,
        pIsAdvanced: TooltipContext?
    ) {
        val maxMedia = getMaxMedia(pStack)
        if (maxMedia <= 0)
            return

        val capacityString = Text.literal(DUST_AMOUNT_FORMATTER.format((maxMedia / MediaConstants.DUST_UNIT.toFloat()).toDouble()))
            .append(Text.literal(" "))
            .append(Text.translatable("hexshield.misc.literal.inf").string)
            .append(Text.literal(" "))
            .append(Text.translatable("hexshield.misc.unit.dust").string)
            .string

        val capacityComponent = Text.empty()
        for (i in capacityString.indices) {
            capacityComponent.append(ColorOrder.createRainbowText(Text.literal("${capacityString[i]}"), pLevel, i))
        }

        pTooltipComponents?.add(Text.translatable("hexshield.tooltip.media_amount", capacityComponent))
    }

    override fun hasGlint(stack: ItemStack?): Boolean {
        return getMedia(stack) / MediaConstants.DUST_UNIT.toFloat() > 15
    }

    override fun getRarity(stack: ItemStack?): Rarity {
        return Rarity.EPIC
    }

    companion object {
        val DUST_AMOUNT_FORMATTER = DecimalFormat("###,###.##")
    }

    override fun getDisplayLabel(stack: ItemStack): String? {
        val media = getMaxMedia(stack)
        if (media <= 0)
            return null
        return Text.translatable("hexshield.misc.literal.inf").string
    }

}