package com.defaulteugene.hexshield.item

import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.item.ItemStack

interface ICustomDisplayInfo {
    fun getDisplayLabel(stack: ItemStack): String? = null
    fun getDisplayLabel(effect: StatusEffectInstance): String? = null
}