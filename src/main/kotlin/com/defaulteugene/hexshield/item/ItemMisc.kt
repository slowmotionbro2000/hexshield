package com.defaulteugene.hexshield.item

import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.util.Rarity

class ItemMisc(settings: Settings, private val rarity: Rarity) : Item(settings) {
    override fun getRarity(stack: ItemStack?): Rarity = rarity
}