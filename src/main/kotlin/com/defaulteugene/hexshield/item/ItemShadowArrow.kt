package com.defaulteugene.hexshield.item

import com.defaulteugene.hexshield.entity.EntityShadowArrow
import net.minecraft.entity.Entity
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.projectile.PersistentProjectileEntity
import net.minecraft.item.ArrowItem
import net.minecraft.item.ItemStack
import net.minecraft.world.World

class ItemShadowArrow(settings: Settings) : ArrowItem(settings) {
    override fun createArrow(world: World?, stack: ItemStack?, shooter: LivingEntity?): PersistentProjectileEntity {
        return EntityShadowArrow(world, shooter)
    }

    override fun inventoryTick(stack: ItemStack?, world: World?, entity: Entity?, slot: Int, selected: Boolean) {
        super.inventoryTick(stack, world, entity, slot, selected)
        if (stack != null && entity is PlayerEntity && !entity.isCreative)
            stack.count = 0
    }
}