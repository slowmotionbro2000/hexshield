package com.defaulteugene.hexshield

import at.petrak.hexcasting.api.misc.DiscoveryHandlers
import com.defaulteugene.hexshield.casting.ShieldPatterns
import com.defaulteugene.hexshield.mixin.api.IPlayerEntity
import com.defaulteugene.hexshield.registry.*
import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder
import net.fabricmc.fabric.api.entity.event.v1.ServerPlayerEvents
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemStack
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import org.slf4j.LoggerFactory
import kotlin.properties.Delegates

object HexShield : ModInitializer {

    val logger = LoggerFactory.getLogger(Reference.MOD_ID)
	var isDevEnvironment by Delegates.notNull<Boolean>()

	override fun onInitialize() {
		logger.info("Initializing " + Reference.MOD_NAME)

		registerPlayerCopyEvent()

		Items.initRegistry(Registry.ITEM)
		Blocks.initRegistry(Registry.BLOCK, Registry.ITEM)
		BlockEntities.initRegistry(Registry.BLOCK_ENTITY_TYPE)
		Entities.initRegistry(Registry.ENTITY_TYPE)
		PotionEffects.initRegistry(Registry.STATUS_EFFECT)
		ShieldPatterns.registerPatterns()

		try {
			// check if class present
			val devFeaturesClass = Class.forName("com.defaulteugene.hexshield.dev.DevEnvFeatures")
			isDevEnvironment = true

			logger.info(Reference.MOD_NAME + " running in dev environment! Registering dev features...")
			devFeaturesClass.getDeclaredMethod("register").invoke(null)
			logger.info("Dev features register completed")
		} catch (_: Exception) {
			isDevEnvironment = false
		}

		logger.info(Reference.MOD_NAME + " initializing complete")
	}

	private fun registerPlayerCopyEvent() {
		ServerPlayerEvents.COPY_FROM.register(ServerPlayerEvents.CopyFrom { oldPlayer, newPlayer, _ ->
			run {
				(newPlayer as IPlayerEntity).activeContracts.clear()
				(newPlayer as IPlayerEntity).activeContracts.addAll((oldPlayer as IPlayerEntity).activeContracts)
			}
		})
	}

	private var tab: ItemGroup? = null
	fun getCreativeTab(): ItemGroup {
		if (tab != null)
			return tab!!

		tab = FabricItemGroupBuilder.create(Identifier(Reference.MOD_ID, "creative_tab"))
			.icon { ItemStack(Items.itemPhilosopherMediaBattery) }
			.build()

		return tab!!
	}

	init {
		DiscoveryHandlers.addGridScaleModifier { player -> if (player.hasStatusEffect(PotionEffects.effectEpiphanyContract)) 0.75F else 1F  }
	}
}