package com.defaulteugene.hexshield.mixin.api;

public interface IPersistentProjectile {
    int getLife();

    void setLife(int life);
}
