package com.defaulteugene.hexshield.mixin.api;

import java.util.Set;

public interface IPlayerEntity {

    Set<String> getActiveContracts();

    boolean isContractActive(String name);

    void setContractActive(String name, boolean isActive);

    default void toggleContractActive(String name) {
        setContractActive(name, !isContractActive(name));
    }

    boolean tickEverwingsContractCounter(boolean isFlying);
}
