package com.defaulteugene.hexshield.mixin.api;

import net.minecraft.entity.Entity;

public interface ISoulDropEntity {

    void setSoulDropped(boolean dropped);

    boolean getSoulDropped();

    void dropSoul();

    default Entity asEntity() {
        return (Entity) this;
    }

}
