package com.defaulteugene.hexshield.mixin.api;

import com.samsthenerd.hexgloop.screens.PatternStyle;
import net.minecraft.text.ClickEvent;
import net.minecraft.text.Style;

import java.util.regex.Pattern;

public class HexPatternMatcher {
    private static final Pattern hexPattern = Pattern.compile("(<.+,.+>)");

    public static boolean isHexPattern(Style style) {
        if (((PatternStyle) style).getPattern() != null)
            return true;

        ClickEvent evt = style.getClickEvent();
        if (evt == null || evt.getAction() != ClickEvent.Action.COPY_TO_CLIPBOARD)
            return false;

        return hexPattern.matcher(evt.getValue()).matches();
    }
}
