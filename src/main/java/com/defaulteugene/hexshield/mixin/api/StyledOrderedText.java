package com.defaulteugene.hexshield.mixin.api;

import at.petrak.hexcasting.api.spell.math.HexPattern;
import com.samsthenerd.hexgloop.screens.PatternStyle;
import net.minecraft.client.font.TextVisitFactory;
import net.minecraft.text.CharacterVisitor;
import net.minecraft.text.OrderedText;
import net.minecraft.text.Style;
import net.minecraft.text.TextColor;

public class StyledOrderedText {

    public record ForwardsVisitedString(String string, Style style) implements OrderedText {
        @Override
        public boolean accept(CharacterVisitor visitor) {
            return TextVisitFactory.visitForwards(string, style, visitor);
        }

        public void setColor(TextColor color) {
            ((IStyleAccessor) style).setColor(color);
        }

        public HexPattern getHexPattern() {
            return ((PatternStyle) style).getPattern();
        }
    }
}
