package com.defaulteugene.hexshield.mixin.api;

import net.minecraft.text.TextColor;

public interface IStyleAccessor {
    void setColor(TextColor color);
}
