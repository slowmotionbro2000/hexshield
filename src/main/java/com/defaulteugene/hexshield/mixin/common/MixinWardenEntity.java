package com.defaulteugene.hexshield.mixin.common;

import com.defaulteugene.hexshield.mixin.api.ISoulDropEntity;
import com.defaulteugene.hexshield.registry.Items;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.mob.WardenEntity;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

@Mixin(WardenEntity.class)
public class MixinWardenEntity implements ISoulDropEntity {

    private boolean soulDropped = false;

    @Override
    @Unique
    public void setSoulDropped(boolean dropped) {
        soulDropped = dropped;
    }

    @Override
    @Unique
    public boolean getSoulDropped() {
        return soulDropped;
    }

    @Override
    public void dropSoul() {
        Entity entity = asEntity();
        if (entity.isAlive())
            return;

        if (getSoulDropped())
            return;

        setSoulDropped(true);

        entity.world.spawnEntity(
                new ItemEntity(
                        entity.world,
                        entity.getX(), entity.getY(), entity.getZ(),
                        new ItemStack(Items.INSTANCE.getWardenSoul())
                )
        );
    }


}
