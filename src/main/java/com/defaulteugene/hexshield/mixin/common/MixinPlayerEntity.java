package com.defaulteugene.hexshield.mixin.common;

import com.defaulteugene.hexshield.Reference;
import com.defaulteugene.hexshield.entity.data.PlayerDataSacrificialContract;
import com.defaulteugene.hexshield.mixin.api.IPlayerEntity;
import com.defaulteugene.hexshield.utils.MediaCastingUtil;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.HashSet;
import java.util.Set;

@Mixin(PlayerEntity.class)
public abstract class MixinPlayerEntity implements IPlayerEntity {

    @Unique
    private final Set<String> activeContracts = new HashSet<>();
    @Unique
    private long lastTickTime = System.currentTimeMillis();

    @Unique
    private int everwingsContractCounter = 0;

    @Inject(method = "readCustomDataFromNbt", at = @At("TAIL"))
    public void injectReadFromNBT(NbtCompound nbt, CallbackInfo ci) {
        if (nbt == null) return;
        if (!nbt.contains(nbtTag)) return;

        NbtCompound contracts = nbt.getCompound(nbtTag);
        PlayerDataSacrificialContract.INSTANCE.getMapContractToEffect().keySet().forEach(name -> {
                if (contracts.contains(name) && contracts.getBoolean(name)) {
                    activeContracts.add(name);
                } else {
                    activeContracts.remove(name);
                }
        });
    }

    @Inject(method = "writeCustomDataToNbt", at = @At("TAIL"))
    public void injectWriteToNBT(NbtCompound nbt, CallbackInfo ci) {
        if (nbt == null) return;

        NbtCompound contracts = nbt.contains(nbtTag) ? nbt.getCompound(nbtTag) : new NbtCompound();
        PlayerDataSacrificialContract.INSTANCE.getMapContractToEffect().keySet().forEach(name -> contracts.putBoolean(name, activeContracts.contains(name)));

        nbt.put(nbtTag, contracts);
    }

    @Inject(method = "tick", at = @At("HEAD"))
    public void injectTick(CallbackInfo ci) {
        long now = System.currentTimeMillis();
        if (lastTickTime < now - 10000) {
            lastTickTime = now;
            PlayerDataSacrificialContract.INSTANCE.applyContracts(asPlayer());
        }
    }

    @Unique
    private PlayerEntity asPlayer() {
        return (PlayerEntity) (Object) this;
    }

    @Unique private final String nbtTag = Reference.MOD_ID + ":contracts";

    @Override
    public Set<String> getActiveContracts() {
        return activeContracts;
    }

    @Override
    public boolean isContractActive(String name) {
        return activeContracts.contains(name);
    }

    @Override
    public void setContractActive(String name, boolean isActive) {
        StatusEffect effect = PlayerDataSacrificialContract.INSTANCE.getMapContractToEffect().get(name);
        if (effect == null)
            return;
        if (isActive) {
            activeContracts.add(name);
            asPlayer().addStatusEffect(new StatusEffectInstance(effect, 400, 0 ,true, false, true));
        } else {
            activeContracts.remove(name);
            asPlayer().removeStatusEffect(effect);
        }
    }

    @Override
    public boolean tickEverwingsContractCounter(boolean isFlying) {
        if ((Object)this instanceof ServerPlayerEntity player) {

            int toGet = this.everwingsContractCounter;

            // if player stops flying we need to consume all counted media
            if (!isFlying) {
                this.everwingsContractCounter = 0;
                return MediaCastingUtil.INSTANCE
                        .withdrawMediaDirect(player, toGet, true, false) <= 0;
            }

            // else if counter less than 1200, just check if we can draw and increase counter
            if (this.everwingsContractCounter < 1200
                    && MediaCastingUtil.INSTANCE
                    .withdrawMediaDirect(player, toGet, false, true) <= 0) {

                this.everwingsContractCounter++;
                return true;
            }

            // else if counter greater than 1200, or we can't draw counted media then just draw it with overcast
            this.everwingsContractCounter = 0;

            // check how much we can get without overcast
            toGet = MediaCastingUtil.INSTANCE
                    .withdrawMediaDirect(player, toGet, false, false);
            if (toGet <= 0)
                return true;

            // if we need to overcast then consume HP and return false - flying with overcast is restricted
            MediaCastingUtil.INSTANCE
                    .withdrawMediaDirect(player, toGet, true, false);

            return false;
        }

        // if code running on logical client side then just return
        return true;
    }
}
