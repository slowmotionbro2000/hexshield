package com.defaulteugene.hexshield.mixin.common;

import com.defaulteugene.hexshield.mixin.api.IPersistentProjectile;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(PersistentProjectileEntity.class)
public abstract class AccessorPersistentProjectileEntity implements IPersistentProjectile {

    @Override
    @Accessor(value = "life")
    public abstract int getLife();

    @Override
    @Accessor(value = "life")
    public abstract void setLife(int life);

}
