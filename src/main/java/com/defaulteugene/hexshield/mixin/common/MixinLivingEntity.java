package com.defaulteugene.hexshield.mixin.common;

import at.petrak.hexcasting.api.misc.HexDamageSources;
import com.defaulteugene.hexshield.registry.PotionEffects;
import com.google.common.collect.ImmutableList;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Collection;

@Mixin(LivingEntity.class)
@SuppressWarnings("ConstantConditions")
public abstract class MixinLivingEntity {

    @Shadow public abstract Collection<StatusEffectInstance> getStatusEffects();
    @Shadow public abstract boolean hasStatusEffect(StatusEffect effect);
    @Shadow public abstract boolean damage(DamageSource source, float amount);

    @Shadow protected abstract void fall(double heightDifference, boolean onGround, BlockState state, BlockPos landedPosition);

    @Inject(method = "onAttacking", at = @At("HEAD"))
    public void injectOnAttaching(Entity target, CallbackInfo ci) {
        if (this.equals(target))
            return;

        if ((Object) this instanceof PlayerEntity player && target instanceof LivingEntity living) {
            if (player.getHealth() > 0 && player.hasStatusEffect(PotionEffects.INSTANCE.getEffectVampireContract())) {
                player.setHealth(player.getHealth() + 2);
            }

            if (player.hasStatusEffect(PotionEffects.INSTANCE.getEffectCursedContract())) {
                living.addStatusEffect(new StatusEffectInstance(effects[player.world.random.nextInt(effects.length)], 200, 3), player);
            }
        }

        if (target instanceof PlayerEntity player && player.hasStatusEffect(PotionEffects.INSTANCE.getEffectCursedContract())) {
            getStatusEffects().forEach(effect -> {
                if (effect.getEffectType().getCategory() == StatusEffectCategory.HARMFUL)
                    player.addStatusEffect(new StatusEffectInstance(effect.getEffectType(), 100, 0));
            });
        }
    }

    @Inject(method = "damage", at = @At("HEAD"), cancellable = true)
    public void injectDamage(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (!hasStatusEffect(PotionEffects.INSTANCE.getEffectResistanceContract()))
            return;

        if (protectedSources.contains(source)) {
            cir.setReturnValue(false);
        } else if (source != DamageSource.GENERIC && !HexDamageSources.OVERCAST.name.equals(source.name)) {
            damage(DamageSource.GENERIC, amount);
        }
    }

    @Inject(method = "heal", at = @At("HEAD"), cancellable = true)
    public void injectHeal(float amount, CallbackInfo ci) {
        if ((Object) this instanceof PlayerEntity player && player.hasStatusEffect(PotionEffects.INSTANCE.getEffectVampireContract()))
            ci.cancel();
    }

    @Unique
    private final StatusEffect[] effects = new StatusEffect[] {
            StatusEffects.POISON,
            StatusEffects.BLINDNESS,
            StatusEffects.WITHER,
            StatusEffects.MINING_FATIGUE,
            StatusEffects.NAUSEA,
            StatusEffects.DARKNESS
    };

    @Unique private final ImmutableList<DamageSource> protectedSources = ImmutableList.of(
            DamageSource.DROWN, DamageSource.LAVA, DamageSource.IN_FIRE, DamageSource.ON_FIRE,
            DamageSource.FREEZE, DamageSource.LIGHTNING_BOLT, DamageSource.HOT_FLOOR, DamageSource.IN_WALL,
            DamageSource.FLY_INTO_WALL, DamageSource.STARVE, DamageSource.FALL, DamageSource.WITHER
    );

}
