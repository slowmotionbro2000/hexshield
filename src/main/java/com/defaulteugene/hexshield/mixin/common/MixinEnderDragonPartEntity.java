package com.defaulteugene.hexshield.mixin.common;

import com.defaulteugene.hexshield.mixin.api.ISoulDropEntity;
import net.minecraft.entity.boss.dragon.EnderDragonPart;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

@Mixin(EnderDragonPart.class)
public class MixinEnderDragonPartEntity implements ISoulDropEntity {

    @Override
    @Unique
    public void setSoulDropped(boolean dropped) {
        throw new IllegalStateException();
    }

    @Override
    @Unique
    public boolean getSoulDropped() {
        throw new IllegalStateException();
    }

    @Override
    public void dropSoul() {
        ((ISoulDropEntity) ((EnderDragonPart) asEntity()).owner).dropSoul();
    }

}
