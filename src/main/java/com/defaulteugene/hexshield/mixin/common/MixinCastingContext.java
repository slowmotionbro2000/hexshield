package com.defaulteugene.hexshield.mixin.common;

import at.petrak.hexcasting.api.spell.casting.CastingContext;
import com.defaulteugene.hexshield.registry.PotionEffects;
import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(CastingContext.class)
public class MixinCastingContext {

    @ModifyExpressionValue(
            method = "isVecInRange",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/util/math/Vec3d;squaredDistanceTo(Lnet/minecraft/util/math/Vec3d;)D")
    )
    public double modifyDistanceToTarget(double original) {
        CastingContext ctx = self();
        if (ctx == null) return original;

        ServerPlayerEntity caster = ctx.getCaster();
        if (caster == null)
            return original;

        if (caster.hasStatusEffect(PotionEffects.INSTANCE.getEffectRangeContract())) {
            return original / 16;
        }

        return original;
    }

    @Unique
    private CastingContext self() {
        return (CastingContext) (Object) this;
    }
}
