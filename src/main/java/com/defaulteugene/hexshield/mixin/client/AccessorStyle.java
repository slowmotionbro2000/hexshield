package com.defaulteugene.hexshield.mixin.client;

import com.defaulteugene.hexshield.mixin.api.IStyleAccessor;
import net.minecraft.text.Style;
import net.minecraft.text.TextColor;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Style.class)
public class AccessorStyle implements IStyleAccessor {

    @Final @Mutable @Shadow TextColor color;
    public void setColor(TextColor color) {
        this.color = color;
    }
}
