package com.defaulteugene.hexshield.mixin.client;

import com.defaulteugene.hexshield.item.ICustomDisplayInfo;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffectUtil;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(StatusEffectUtil.class)
public class MixinStatusEffectUtil {

    @Inject(method = "durationToString", at = @At("HEAD"), cancellable = true)
    private static void durationToString(StatusEffectInstance effect, float multiplier, CallbackInfoReturnable<String> cir) {
        if (!(effect.getEffectType() instanceof ICustomDisplayInfo)) return;

        String label = ((ICustomDisplayInfo) effect.getEffectType()).getDisplayLabel(effect);
        if (label == null) return;

        cir.setReturnValue(label);
    }

}
