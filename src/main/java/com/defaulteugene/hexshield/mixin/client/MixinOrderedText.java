package com.defaulteugene.hexshield.mixin.client;

import at.petrak.hexcasting.api.spell.math.HexPattern;
import com.defaulteugene.hexshield.mixin.api.StyledOrderedText;
import com.defaulteugene.hexshield.utils.ColorOrder;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.text.CharacterVisitor;
import net.minecraft.text.OrderedText;
import net.minecraft.text.Style;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Iterator;
import java.util.List;

@Mixin(OrderedText.class)
@Environment(EnvType.CLIENT)
public interface MixinOrderedText {

    @Inject(
            method = "styledForwardsVisitedString(Ljava/lang/String;Lnet/minecraft/text/Style;)Lnet/minecraft/text/OrderedText;",
            at = @At("HEAD"),
            cancellable = true
    )
    private static void injectStyledClass(String string, Style style, CallbackInfoReturnable<OrderedText> cir) {
        if (!string.isEmpty()) {
            cir.setReturnValue(new StyledOrderedText.ForwardsVisitedString(string, style));
        }
    }

    @Inject(
            method = "method_30750",
            at = @At("HEAD"),
            cancellable = true
    )
    private static void injectLambda$30750(List<OrderedText> texts, CharacterVisitor visitor, CallbackInfoReturnable<Boolean> cir) {
        Iterator<OrderedText> iterator = texts.iterator();
        int parenDepth = 0;
        boolean depthCounts = false;

        OrderedText orderedText;
        do {
            if (!iterator.hasNext()) {
                cir.setReturnValue(true);
                return;
            }

            orderedText = iterator.next();
            if (orderedText instanceof StyledOrderedText.ForwardsVisitedString styled) {
                if ("[".equals(styled.string())) {
                    depthCounts = true;
                } else if ("]".equals(styled.string())) {
                    depthCounts = false;
                    parenDepth = 0;
                }

                if (depthCounts) {
                    HexPattern pattern = styled.getHexPattern();
                    if (pattern != null) {
                        if ("H".equals(styled.string())) {
                            int additionalDepth = 0;

                            String sp = pattern.anglesSignature();
                            if ("qqq".equals(sp))
                                additionalDepth = 1;
                            else if ("eee".equals(sp))
                                additionalDepth = -1;

                            if (additionalDepth < 0)
                                parenDepth += additionalDepth;

                            styled.setColor(ColorOrder.INSTANCE.getTextColorByIndex(parenDepth));

                            if (additionalDepth > 0)
                                parenDepth += additionalDepth;

                        } else if (!styled.string().contains("[") && !styled.string().contains("]")) {
                            styled.setColor(ColorOrder.INSTANCE.getTextColorByIndex(parenDepth));
                        }
                    }
                }
            }
        } while (orderedText.accept(visitor));

        cir.setReturnValue(false);
    }
}
