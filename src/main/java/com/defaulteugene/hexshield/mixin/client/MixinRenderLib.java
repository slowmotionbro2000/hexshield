package com.defaulteugene.hexshield.mixin.client;

import at.petrak.hexcasting.client.RenderLib;
import com.defaulteugene.hexshield.utils.ColorOrder;
import net.minecraft.util.math.Matrix4f;
import net.minecraft.util.math.Vec2f;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.awt.*;
import java.util.List;

@Mixin(value = RenderLib.class, priority = 500)
public class MixinRenderLib {

    @Inject(method = "drawLineSeq", at=@At("HEAD"), cancellable = true)
    private static void mixinDrawLineSeq(Matrix4f mat, List<? extends Vec2f> points, float width, float z, int tail, int head, CallbackInfo ci) {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();

        // check if method called exactly from text drawer, not from another place
        boolean calledByPatStyDrawer = false;
        boolean calledByMixin = false, calledByMixinTwice = false;
        for (StackTraceElement stackTraceElement : stackTrace) {
            String methodName = stackTraceElement.getMethodName();
            if (methodName.contains("mixinDrawLineSeq")) {
                if (!calledByMixin)
                    calledByMixin = true;
                else
                    calledByMixinTwice = true;
            }
            if (methodName.contains("PatStyDrawerAccept")) {
                calledByPatStyDrawer = true;
                break;
            }
        }

        // prevent modifying non-HexGloop call or recursive call
        if (!calledByPatStyDrawer || calledByMixinTwice)
            return;

        if (z <= 0) {
            // cancel calling an 0-index drawer that draws white background for lines
            ci.cancel();
        } else {
            // we canceled drawing 0-index line, so let's re-draw it
            int colorHead = ColorOrder.INSTANCE.setColorBrightness(new Color(head & 0x00ffffff), 1).getRGB() & 0x00ffffff | 0x80000000;
            int colorTail = ColorOrder.INSTANCE.setColorBrightness(new Color(tail & 0x00ffffff), 1).getRGB() & 0x00ffffff | 0x80000000;
            RenderLib.drawLineSeq(mat, points, width / 0.4f, 0, colorTail, colorHead);
        }
    }
}
