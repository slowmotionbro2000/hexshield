package com.defaulteugene.hexshield.mixin.client;

import com.defaulteugene.hexshield.item.ICustomDisplayInfo;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.render.LightmapTextureManager;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ItemRenderer.class)
public class MixinItemRenderer {

    @Shadow
    public float zOffset;

    @Inject(
            method = "renderGuiItemOverlay(Lnet/minecraft/client/font/TextRenderer;Lnet/minecraft/item/ItemStack;IILjava/lang/String;)V",
            at = @At("HEAD"),
            cancellable = true
    )
    public void injectRenderGuiItemOverlay(TextRenderer renderer, ItemStack stack, int x, int y, String countLabel, CallbackInfo ci) {
        if (countLabel != null) return;
        if (!(stack.getItem() instanceof ICustomDisplayInfo)) return;

        String label = ((ICustomDisplayInfo) stack.getItem()).getDisplayLabel(stack);
        if (label == null) return;

        ci.cancel();

        MatrixStack matrixStack = new MatrixStack();
        matrixStack.translate(0.0, 0.0, this.zOffset + 200.0f);
        VertexConsumerProvider.Immediate immediate = VertexConsumerProvider.immediate(Tessellator.getInstance().getBuffer());
        renderer.draw(label, (float) (x + 19 - 2 - renderer.getWidth(label)), (float) (y + 6 + 3), 0xFFFFFF, true, matrixStack.peek().getPositionMatrix(), immediate, false, 0, LightmapTextureManager.MAX_LIGHT_COORDINATE);
        immediate.draw();
    }
}
