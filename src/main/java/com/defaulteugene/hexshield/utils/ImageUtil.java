package com.defaulteugene.hexshield.utils;

import at.petrak.hexcasting.api.spell.iota.*;
import com.defaulteugene.hexshield.utils.iota_images.*;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.Vec2f;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

@Environment(EnvType.CLIENT)
public class ImageUtil {

    private static final Map<IotaType<?>, BiFunction<Iota, IotaType<?>, AbstractAdapter>> adapterMapPre = new HashMap<>();
    private static final Map<IotaType<?>, BiFunction<Iota, IotaType<?>, AbstractAdapter>> adapterMapPost = new HashMap<>();

    static {
        // this adapters SHOULD have iota=null
        adapterMapPre.put(EntityIota.TYPE, (iota, type) -> EntityAdapter.INSTANCE);
        adapterMapPre.put(GarbageIota.TYPE, (iota, type) -> GarbageAdapter.INSTANCE);
        adapterMapPre.put(NullIota.TYPE, (iota, type) -> GarbageAdapter.INSTANCE);

        // iota=null check is here

        // this adapters should NOT have iota=null
        adapterMapPost.put(BooleanIota.TYPE, (iota, type) -> BooleanAdapter.Companion.adapt(((BooleanIota) iota).getBool()));
        adapterMapPost.put(DoubleIota.TYPE, (iota, type) -> new NumberAdapter(((DoubleIota) iota).getDouble()));
        adapterMapPost.put(PatternIota.TYPE, (iota, type) -> new HexPatternAdapter(((PatternIota) iota).getPattern()));
        adapterMapPost.put(Vec3Iota.TYPE, (iota, type) -> new Vec3Adapter(((Vec3Iota) iota).getVec3()));
    }

    public static BufferedImage iotaToImage(IotaWrapper wrapper) {

        if (wrapper == IotaWrapper.OPEN_BRACE)
            return BraceAdapter.Companion.getLeft().createImage();

        if (wrapper == IotaWrapper.CLOSE_BRACE)
            return BraceAdapter.Companion.getRight().createImage();

        BiFunction<Iota, IotaType<?>, AbstractAdapter> adapter = adapterMapPre.get(wrapper.type);
        if (adapter != null)
            return adapter.apply(wrapper.iota, wrapper.type).createImage();

        if (wrapper.iota == null)
            return new IotaIsNullAdapter(wrapper.type).createImage();

        adapter = adapterMapPost.getOrDefault(wrapper.type, (iota, type) -> new UnknownAdapter(type));
        return adapter.apply(wrapper.iota, wrapper.type).createImage();
    }

    public static void fillArrow(Graphics2D g, Vec2f start, Vec2f finish, float position) {
        Vec2f arrowPos = finish.add(start.negate()).multiply(position).add(start);
        int size = (int) Math.round(Math.sqrt(finish.add(start.negate()).lengthSquared()) / 10);

        drawArrowLine(g,
                Math.round(start.x), Math.round(start.y),
                Math.round(arrowPos.x), Math.round(arrowPos.y),
                size, size
        );
    }

    public static void drawArrowLine(Graphics2D g, int x1, int y1, int x2, int y2, int d, int h) {
        int dx = x2 - x1, dy = y2 - y1;
        double D = Math.sqrt(dx * dx + dy * dy);
        double xm = D - d, xn = xm, ym = h, yn = -h, x;
        double sin = dy / D, cos = dx / D;

        x = xm * cos - ym * sin + x1;
        ym = xm * sin + ym * cos + y1;
        xm = x;

        x = xn * cos - yn * sin + x1;
        yn = xn * sin + yn * cos + y1;
        xn = x;

        int[] xpoints = {x2, (int) xm, (int) xn};
        int[] ypoints = {y2, (int) ym, (int) yn};

        g.drawLine(x1, y1, x2, y2);
        g.fillPolygon(xpoints, ypoints, 3);
    }

    public static BufferedImage withCaption(BufferedImage source, String caption) {

        int hSource = source.getHeight();
        int hFont = caption == null ? 0 : (int) Math.ceil(hSource * 0.2f);
        int hDesc = (int) Math.ceil(hSource * 0.05f);
        BufferedImage newImage = new BufferedImage(source.getWidth(), hSource + hFont + hDesc * 2, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics = newImage.createGraphics();

        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, newImage.getWidth(), newImage.getHeight());
        graphics.drawImage(source, 0, hFont, (img, infoflags, x, y, width, height) -> true);

        Font defaultFont = graphics.getFont();

        if (hFont > 0) {
            graphics.setColor(new Color(Formatting.DARK_PURPLE.getColorValue()));

            int actualHFont = (int) Math.floor(hFont * 0.9f);
            Font newFont;
            boolean great = false;
            do {
                newFont = new Font(defaultFont.getFontName(), defaultFont.getStyle(), actualHFont);
                graphics.setFont(newFont);

                if (graphics.getFontMetrics().stringWidth(caption) <= source.getWidth() * 0.9)
                    great = true;
                else
                    actualHFont = (int) Math.floor(actualHFont * 0.9f);
            } while (!great);

            int w = graphics.getFontMetrics().stringWidth(caption);
            graphics.drawString(caption, source.getWidth() / 2 - w / 2, hFont / 2 + actualHFont / 3);
        }

        int actualHDesc = (int) Math.floor(hDesc * 0.9f);
        graphics.setFont(new Font(defaultFont.getFontName(), defaultFont.getStyle(), actualHDesc));
        graphics.setColor(new Color(Formatting.DARK_AQUA.getColorValue()));
        graphics.drawString(
                Text.translatable("hexshield.misc.hexdump.author").getString() + MinecraftClient.getInstance().player.getName().getString(),
                actualHDesc / 3,
                newImage.getHeight() - hDesc / 2 + actualHDesc / 3
        );

        return newImage;
    }

    public static BufferedImage combineImages(int minInRow, BufferedImage[] images) {
        if (images.length < minInRow)
            minInRow = images.length;

        int height = (int) Math.floor(Math.sqrt(images.length));
        int width = images.length / height + (images.length % height > 0 ? 1 : 0);

        if (width < minInRow) {
            width = minInRow;
            height = images.length / width + (images.length % width > 0 ? 1 : 0);
        }

        BufferedImage resultImage = new BufferedImage(1024 * width, 1024 * height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics = resultImage.createGraphics();

        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, 1024 * width, 1024 * height);

        drawer:
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (i * width + j >= images.length)
                    break drawer;

                graphics.drawImage(images[i * width + j], j * 1024, i * 1024, (img, infoflags, x, y, width1, height1) -> true);
            }
        }

        return resultImage;
    }

    /**
     * WARNING: DO NOT PASS NULL AS TYPE ARGUMENT
     * Brace wrappers are CONSTANT, their fields won't been taken whatever
     */
    @SuppressWarnings("ConstantConditions")
    public static class IotaWrapper {

        public static final IotaWrapper OPEN_BRACE = new IotaWrapper(null, null) {
        }; // [
        public static final IotaWrapper CLOSE_BRACE = new IotaWrapper(null, null) {
        }; // ]
        public final Iota iota;
        public final IotaType<?> type;

        public IotaWrapper(Iota iota, @NotNull IotaType<?> type) {
            this.iota = iota;
            this.type = type;
        }
    }

}
