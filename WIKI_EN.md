### Change language: 
#### **English (you are here) |** [Русский](https://gitlab.com/slowmotionbro2000/hexshield/-/blob/main/WIKI_RU.md)

<a name="contents"></a>
# Contents

* [Global changes](#changes)
  * [Colored Iotas in descriptions](#changes_colored_runes)
  * [Spells to images](#changes_hexdump)
* [Items](#items)
  * [Phial of Eternal Mind](#items_philosopher_battery)
  * [Souls of Strongest](#items_strongest_souls)
  * [Mobithysts](#items_mobithysts)
  * [Pandora's Box](#items_media_storage)
* [Patterns](#patterns)
  * [Surgeon's Exaltation II](#patterns_multiple_modify_in_place) 
* [Spells](#spells)
  * [Heart Whisper](#spells_heartwhisper)
  * [Spectral Weapon Projection](#spells_spectralarrow)
  * [Creating a Phial of Eternal Mind](#spells_create_philosopher_battery)
  * [Creating a Wardenyst](#spells_create_wardenyst)
  * [Creating a Dragonyst](#spells_create_dragonyst)
* [Sacrificial contracts](#contracts)
  * [Vampire contract](#contracts_vampire) 
  * [Epiphany contract](#contracts_epiphany) 
  * [Resistance contract](#contracts_resistance) 
  * [Everwings contract](#contracts_everwings) 
  * [Cursed contract](#contracts_cursed)
  * [Freedom contract](#contracts_range)




---
<a name="changes"></a>
# [↑](#contents) Global changes

<a name="changes_colored_runes"></a>
## Colored Iotas in descriptions
This add-on made tooltips more readable. You can see on screenshot that Iotas inside the list colored to blue.<br>
If you define a list inside a list inside a list etc., each new list depth level will be colored into another color. So you can easily find where list ends
<details>  

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/tooltips.png)
</details>

<a name="changes_hexdump"></a>
## Spells to images
If you've ever wanted (or will ever want) to share your spells with other people, use this feature!<br>
With any item containing an iota (book, focus, abacus, or even an artifact) in your hand, write the command `/hexdump`<br>
After that, a picture with this spell and step-by-step instructions on how to draw it will appear in your screenshots folder

You can also specify a title for this picture, and if you forget - for renamed items their names will become titles themselves!
<details>  

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/hexdump_example.png)
</details>

---
<a name="items"></a>
# [↑](#contents) Items

<a name="items_philosopher_battery"></a>
## Phial of Eternal Mind
#### An endless stream of consciousness
Have you ever wondered how difficult it is to manage an endless stream of thought?<br><br>It's time to think about it!<br><br>By creating an Phial of Eternal Mind, you will gain an inexhaustible but limited source of magical energy<br><br>Be careful: creating an Phial of Eternal Mind will cost you a lot...

<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/spells/philosopher_battery.png)

</details>

<a name="items_strongest_souls"></a>
## Souls of Strongest
#### Keep your enemies' souls
Well, there really are worthy opponents in this world. And as a memento of my battle with them, I will keep their souls<br><br>Maybe later I can use those souls to create something truly powerful...

Normally when a creature is killed, its soul splits, but what if you use a special weapon? <br><br>Shadow arrows created by the spell [Spectral Weapon Projection](#spells_spectralarrow) are capable of dealing damage to a creature's shadow. If this damage is critical, the creature dies, but its soul remains intact

<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/items/warden_soul.png)
![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/items/dragon_soul.png)

</details>

<a name="items_mobithysts"></a>
## Mobithysts
#### How capacious is the Soul?
Perhaps you'd like to expand your energy storage. But to do that, you'd have to have more capacious items, wouldn't you?<br><br>Mobithysts are Gloops filled with the energy of the world's strongest. You can, of course, use them as items, but it will be much more profitable to make a vault based on them

You can read more about creating Mobithysts in the spellcasting sections [Creating a Wardenyst](#spells_create_wardenyst) and [Creating a Dragonyst](#spells_create_dragonyst)

<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/items/wardenyst.png)
![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/items/dragonyst.png)

</details>

<a name="items_media_storage"></a>
## Pandora's Box
#### Infinite storage for your amethysts!
Perhaps, if you have advanced far enough in the study of your mind, you have already faced the problem of how to store a large number of amethysts, gloops and other items that store media.<br><br>This block will permanently solve the problem of storing items. You can load items into it using a hopper or any other mechanisms, and it will store them in an almost unlimited amount<br><br>In addition, you can easily move it to another location - when you break a block, all its contents will be saved

In order to remove the items previously placed in it, you need to select a page - press the RCB to switch the selected slot. After that, Pandora's Box will push items from the selected slot into the inventory under it (chest, stove, shulker - whatever)<br><br>Hint: Using the Epiphany Lens you can see which slot is currently selected and how many items are stored

Note: This block is only able to hold items that store media. And only consumables - amethyst dust, amethysts, gloops, wardenysts, and so on. You won't be able to store your phials or artifacts in it.<br><br>Hint: you can disable items pushing. Just provide redstone signal to this block

<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/items/media_storage.png)

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/items/media_storage_craft.png)

</details>


---
<a name="patterns"></a>
# [↑](#contents) Patterns

<a name="patterns_multiple_modify_in_place"></a>
## Surgeon's Exaltation II
#### `list : source`, `num : place`, `list | any : modification` → `list`
Remove the top iota or list of the stack and the number at the top, then set the nth element of the list at the top of the stack to that iota or list content (where n is the number you removed). Does nothing if the number is out of bounds.

Differs from [Surgeon's Exaltation][hex_modify_in_place] in that if you passed list as third argument then Surgeon's Exaltation II unwraps it instead of placing a whole list in given place

<details>

Example:


[Surgeon's Exaltation][hex_modify_in_place]:
> [ a b c ]<br>
> 1<br>
> [ d e f ]<br>
> → [ a [ d e f ] c ]

Surgeon's Exaltation II:
> [ a b c ]<br>
> 1<br>
> [ d e f ]<br>
> → [ a d e f c ]

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/pattern_multiple_modify_in_place.png)

</details>


---
<a name="spells"></a>
# [↑](#contents) Spells

<a name="spells_heartwhisper"></a>
## Heart Whisper
#### `entity : target` → `None`
Show your love to your friend (or your enemy, idk) with this spell! Using on someone releases heart particles around target<br><br>Costs nothing because love is not for sale
<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/spells/spell_heartwhisper.png)

</details>

<a name="spells_spectralarrow"></a>
## Spectral Weapon Projection
#### `vec : spawnPos`, `vec : motion`, `bool : critical` → `None`
The powerful spell which allows you to summon a shadow arrow with a given position and motion vec. You also need to specify if arrow should cause critical damage<br><br>Using cost relates in |motion|^3 and critical<br><br>This spell might cost so much...
<details>

Spell casting cost example:
> |mot| = 1, critical = false → 0.06<br>
> |mot| = 1, critical = true → 0.12<br>
> |mot| = 10, critical = false → 10.05<br>
> |mot| = 20.05, critical = false → 80.05<br>

> |mot| = N, critical = false → 0.05 + 0.01 * N^3<br>
> |mot| = N, critical = true → 0.1 + 0.02 * N^3

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/spells/spell_spectralarrow.png)

</details>

<a name="spells_create_philosopher_battery"></a>
## Creating a Phial of Eternal Mind
#### `item : phial`, `num : capacity (in dusts)` → `item: Phial of Eternal Mind`
Creating a Phial of Eternal Mind is somewhat similar to creating a normal phial, but there are some differences<br><br>This spell takes an item - a normal thought bubble and a number - the desired capacity of the Phial of Eternal Mind as its arguments

Unlike crafting a normal phial, there is no need to hold anything in your second hand when using this spell<br><br>In addition, the capacity of a normal phial used for crafting has no effect on anything, nor does the current amount of energy it contains

However, be careful: creating a Phial of Eternal Mind is expensive, very, very expensive, and also has a limit - the maximum capacity of such a phial is 20 (though, it is unlikely that you will be able to create such a phial...)<br><br>Luckily, if you do not have enough energy, the spell will not kill you, but will only warn you about how much is needed to create the desired capacity
<details>

Spell casting cost example:
> capacity = 0.01 → 628.75<br>
> capacity = 0.1 → 663.35<br>
> capacity = 1 → 1093.12<br>
> capacity = 2 → 1785.06<br>
> capacity = 5 → 5861.81<br>
> capacity = 10 → 24414.06<br>
> capacity = 20 → 160000<br>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/spells/spell_philosopher_battery.png)

</details>

<a name="spells_create_wardenyst"></a>
## Creating a Wardenyst
#### `item : Warden Soul` → `item: Wardenyst`
After discovering the ability to save [the Souls of the Strongest](#items_strongest_souls), you thought about their use<br><br>Over time, you found a way to combine 8 Gloops into a single item, the Wardenyst

Wardenyst is the same media item as amethyst or gloop, but has a much larger energy capacity of 8 crystals. As you might have already guessed, you can use it to create more capacious Phials of Mind

For Wardenyst creating you need to:
- Take 8 Gloops in your hand
- Take a Warden Soul to stack as Entity
- Cast the Creating a Wardenyst spell

<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/spells/spell_wardenyst.png)

</details>

<a name="spells_create_dragonyst"></a>
## Creating a Dragonyst
#### `item : Dragon Soul` → `item: Dragonyst`
As you continue to study the effects of [the Souls of the Strongest](#items_strongest_souls), you have discovered a way to use Dragon Soul<br><br>Now you know you can use it to combine 8 Wardenysts to get a Dragonyst

Dragonyst is a compressed version of Wardenyst, with an even larger amount of energy equal to 64 amethyst crystals. Of course, you can also use it to create Phials of Mind, too

For Dragonyst creating you need to:
- Take 8 Wardenysts in your hand
- Take a Dragon Soul to stack as Entity
- Cast the Creating a Dragonyst spell

<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/spells/spell_dragonyst.png)

</details>




---
<a name="contracts"></a>
# [↑](#contents) Sacrificial contracts
A special spells which have permanent effect on player and grants some buffs, but have harmful side effects and a big casting cost<br><br>Cast Sacrificial contract spell again to broke signed contract

<a name="contracts_vampire"></a>
## Vampire contract
#### `None` → `None`
Once you signed this contract you will obtain great damage buff (x2.5 on base damage) and health regen on attack, but you won't have natural health regen anymore (inc. Regeneration or Instant health potions)<br><br>Costs 2000 amethyst dust
<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/contracts/vampire.png)

</details>

<a name="contracts_epiphany"></a>
## Epiphany contract
#### `None` → `None`
Once you signed this contract your Staff casting field will be scaled to x0.75 (like if you holding lens). Have no side affects, but...<br><br>Costs 5000 amethyst dust
<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/contracts/epiphany.png)

</details>

<a name="contracts_resistance"></a>
## Resistance contract
#### `None` → `None`
Once you signed this contract you will obtain full immunity to many damage source, but if you received damage from non-immune source, it will be doubled.<br><br>Costs 2000 amethyst dust
<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/contracts/resistance.png)

</details>

<a name="contracts_everwings"></a>
## Everwings contract
#### `None` → `None`
Once you signed this contract you will obtain creative-like fly, but it will cost 0.12 amethyst dust per minute in air<br><br>Costs 2000 amethyst dust<br><br>
*Note: in v1.0.3b requires holding Iota holding item in hand (like staff or spell book)*<br>
*In 1.0.4b or greater contract don't require it anymore*
<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/contracts/everwings.png)

</details>

<a name="contracts_cursed"></a>
## Cursed contract
#### `None` → `None`
Once you signed this contract every your attack will cause debuffs on enemy, but when you received any attack you will obtain all enemy debuffs<br><br>Costs 2000 amethyst dust
<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/contracts/cursed.png)

</details>

<a name="contracts_range"></a>
## Freedom contract
#### `None` → `None`
Perhaps one of the most freedom-loving contracts. It able to give you more space<br>Once you signed this contract your casting range will be permanently increased by 4 times<br><br>Costs 5000 amethyst dust
<details>

![](https://gitlab.com/slowmotionbro2000/hexshield/-/raw/main/images/contracts/range.png)

</details>

[hex_modify_in_place]: https://hexcasting.hexxy.media/v/0.10.3/1.0.dev21/en_us/#patterns/lists@hexcasting:modify_in_place